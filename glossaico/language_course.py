# SPDX-FileCopyrightText: 2023 Dimitris Kardarakos <dimkard@posteo.net>
# SPDX-License-Identifier: GPL-3.0-or-later

from pathlib import Path
import pickle
import traceback
import sys
import logging
from librelingo_types import Course
from librelingo_yaml_loader import load_course


class CourseLoader:
    """Class that loads a language course"""

    # pylint: disable=too-few-public-methods
    def __init__(
        self, cache_dir: str, course_config_dir: str, installed_data_check_sum: str
    ):
        self._cache_dir = cache_dir
        self._course_config_dir = course_config_dir
        self._installed_checksum = installed_data_check_sum

    def load(self, course_name: str):
        """Loads the data of the language course provided"""

        course_yaml_path = f"{self._course_config_dir}/{course_name}/course"
        try:
            course_cache = CourseCache(
                self._cache_dir, course_name, self._installed_checksum
            )

            if course_cache.exists():
                return course_cache.load()

            course: Course = load_course(course_yaml_path)
            course_cache.save(course)
            return course
        except FileNotFoundError:
            logging.error(traceback.format_exc())
            logging.error("Course file cannot be found")
            sys.exit(1)


class CourseCache:
    """Provides an interface to a binary cache for language courses"""

    def __init__(self, cache_dir: str, course_name: str, installed_data_check_sum: str):
        self._cache_dir = cache_dir
        self._course_name = course_name
        self._installed_checksum = installed_data_check_sum

    def exists(self) -> bool:
        """Returns True if a cached version of the course exists"""
        pickled_course_path = self._pickledCoursePath(self._course_name)

        if pickled_course_path.is_file():
            return True

        return False

    def load(self) -> Course:
        """Loads and returns a course form the binary cache"""
        pickled_course_path = self._pickledCoursePath(self._course_name)

        with open(pickled_course_path, "rb") as pf:
            try:
                loaded_course = pickle.load(pf)
            except pickle.UnpicklingError as exc:
                raise pickle.UnpicklingError(
                    f"{pickled_course_path} is invalid. Please remove it"
                ) from exc
        if not isinstance(loaded_course, Course):
            raise TypeError("Object in cache is not a LibreLingo Course")

        return loaded_course

    def save(self, course: Course):
        """Saves a course to the binary cache"""
        pickled_course_path = self._pickledCoursePath(self._course_name)

        with open(pickled_course_path, "wb") as pf:
            pickle.dump(course, pf)

    def _pickledCoursePath(self, course_name: str) -> Path:
        pickle_dir_path = Path(self._cache_dir, "binary", self._installed_checksum)

        if not pickle_dir_path.is_dir():
            pickle_dir_path.mkdir(parents=True)

        return Path(pickle_dir_path, f"{course_name}.pickle")
