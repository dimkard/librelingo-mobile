# SPDX-FileCopyrightText: 2023 Dimitris Kardarakos <dimkard@posteo.net>
# SPDX-License-Identifier: GPL-3.0-or-later


from sqlalchemy.future import Engine
from sqlalchemy.orm import Session
from glossaico.database_store_keeper import DatabaseStoreKeeper
from glossaico.practice_session_alchemy_store import PracticeSessionAlchemyStore


class PracticeSessionAlchemyStoreKeeper(DatabaseStoreKeeper):
    """Class responsible to maintain a practice session database store"""

    def __init__(self, db_engine: Engine):
        self._db_engine = db_engine
        self._db_session = None
        self._store = None

    def __enter__(self):
        self._db_session = Session(self._db_engine)
        self._store = PracticeSessionAlchemyStore(self._db_session)
        return super().__enter__()

    def __exit__(self, *args):
        super().__exit__(*args)
        self._db_session.close()

    def commit(self):
        self._db_session.commit()

    def rollback(self):
        self._db_session.rollback()

    def store(self):
        return self._store
