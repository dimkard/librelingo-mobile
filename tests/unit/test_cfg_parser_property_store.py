# SPDX-FileCopyrightText: 2022 Dimitris Kardarakos <dimkard@posteo.net>
# SPDX-License-Identifier: GPL-3.0-or-later

import pytest
from glossaico.cfg_parser_property_store import CfgParserPropertyStore
from glossaico.app_configuration import ApplicationConfiguration


@pytest.fixture(scope="function", name="ready_store")
def fixture_ready_store(tmp_path_factory):
    """Provide a ready to use property store"""
    store_loc = tmp_path_factory.mktemp("store_loc")
    store = CfgParserPropertyStore(store_loc)
    store.load("ready-store")
    return store


def test_load_inp_valid_store_name_and_loc_out_loaded_prop_is_true(tmp_path_factory):
    store_home = tmp_path_factory.mktemp("store_home")
    store = CfgParserPropertyStore(store_home)
    store.load("test-load-store")
    assert store.loaded


def test_get_inp_no_config_out_default_vals(ready_store):
    loaded_app_cfg = ready_store.get()
    default_cfg = ApplicationConfiguration()

    assert (
        (loaded_app_cfg.installedDataCheckSum == default_cfg.installedDataCheckSum)
        and (loaded_app_cfg.configVersion == default_cfg.configVersion)
        and (loaded_app_cfg.silentMode == default_cfg.silentMode)
        and (loaded_app_cfg.showBetaCourses == default_cfg.showBetaCourses)
    )


def test_upsert_inp_valid_config_mode_out_no_error(ready_store):
    app_cfg = ApplicationConfiguration("efgfe", True, "1.2", False)
    ready_store.upsert(app_cfg)


def test_get_inp_valid_config_mode_out_correct_vals(ready_store):
    app_cfg = ApplicationConfiguration("dfad", True, "1.3", True)
    ready_store.upsert(app_cfg)
    loaded_app_cfg = ready_store.get()
    assert (
        (loaded_app_cfg.installedDataCheckSum == app_cfg.installedDataCheckSum)
        and (loaded_app_cfg.configVersion == app_cfg.configVersion)
        and (loaded_app_cfg.silentMode == app_cfg.silentMode)
        and (loaded_app_cfg.showBetaCourses == app_cfg.showBetaCourses)
    )
