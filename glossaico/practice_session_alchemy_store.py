# SPDX-FileCopyrightText: 2023 Dimitris Kardarakos <dimkard@posteo.net>
# SPDX-License-Identifier: GPL-3.0-or-later

from sqlalchemy.orm import Session
from glossaico.database_store import DatabaseStore
from glossaico.practice_session_data import PracticeSessionData


class PracticeSessionAlchemyStore(DatabaseStore):
    """Database store that manages the storage of skill progress"""

    def __init__(self, db_session: Session):
        self._session: Session = db_session

    def get(self, record_id: str):
        return self._session.query(PracticeSessionData).filter_by(id=record_id).first()

    def add(self, model: PracticeSessionData):
        self._session.add(model)

    def getBySkillId(self, skill_id: str, course_name: str) -> list:
        """Get the practice sessions of a skill of a course"""
        return (
            self._session.query(PracticeSessionData)
            .filter_by(course=course_name, skill_id=skill_id)
            .all()
        )
