/*
 * SPDX-FileCopyrightText: 2023 Dimitris Kardarakos <dimkard@posteo.net>
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

import QtQuick 2.12
import QtQuick.Controls 2.12 as Controls
import QtQuick.Layouts 1.12
import org.kde.kirigami 2.12 as Kirigami
import org.codeberg.dimkard.glossaico 1.0 as Glossaico

AbstractChallengePage {
    id: root

    challengeItem: cardGridView
    title: QGlTr._("Which of these is ...")
    implicitWidth: pageStack.width
    activeFocusOnTab: false
    answerGiven: (cardGridView.currentIndex == -1) ? { "answer": null } : { "answer" : cardGridView.model[cardGridView.currentIndex].formInTargetLanguage }

    Kirigami.CardsGridView {
        id: cardGridView

        anchors.fill: parent
        model: root.challengeData.options
        activeFocusOnTab: true
        focus: true
        keyNavigationEnabled: true
        currentIndex: -1

        header: Kirigami.Heading {
            text: QGlTr._("Which of these is <b>%1</b>?").arg(root.challengeData.meaningInSourceLanguage)
            wrapMode: Text.WordWrap
            width: parent.width
            bottomPadding: Kirigami.Units.largeSpacing * 2
            leftPadding: Kirigami.Units.largeSpacing
        }

        cellWidth: Math.floor(width/columns)
        cellHeight: 240 + Kirigami.Units.gridUnit * 5 // The pixel size of the _tiny images used in the delegate is 240x240

        maximumColumns: 2

        delegate: Kirigami.AbstractCard {
            id: card

            showClickFeedback: true
            highlighted: model.index === cardGridView.currentIndex

            header: Kirigami.Heading {
                text: model && modelData.formInTargetLanguage
                wrapMode: Text.WordWrap
                horizontalAlignment: Text.AlignHCenter
            }

            contentItem: Image {
                source: modelData.picture ? QDirSettings.imageDir + "/" + modelData.picture + "_tiny" : ""
                fillMode: Image.PreserveAspectFit
            }

            onClicked: cardGridView.currentIndex = model.index
        }
    }
}
