# SPDX-FileCopyrightText: 2021 Dimitris Kardarakos <dimkard@posteo.net>
# SPDX-License-Identifier: GPL-3.0-or-later

from . import glossaico_app

glossaico_app.main()
