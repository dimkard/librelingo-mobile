# SPDX-FileCopyrightText: 2022 Dimitris Kardarakos <dimkard@posteo.net>
# SPDX-License-Identifier: GPL-3.0-or-later

import pytest
from librelingo_types import Word, Phrase
from glossaico.practice_session_flow import PracticeSessionFlow
from glossaico.language_skill import LanguageSkill
from glossaico.challenge_data_calculator import ChallengeDataCalculator
from glossaico.glossaico_types import SkillData
from glossaico.progress_calculator import ProgressCalculator
from glossaico.course_validation import CourseValidation


@pytest.fixture(scope="function", name="validator")
def fixture_validator():

    course_validation = CourseValidation(str(), str())
    course_validation.imageExists = lambda x, y: True
    course_validation.voiceExists = lambda x, y: True

    return course_validation


@pytest.fixture(scope="function", name="valid_model")
def fixture_valid_model():
    challenge_calculator = ChallengeDataCalculator("", None, 3, 4)
    progress_calculator = ProgressCalculator(None)

    def dummyValidateLoad():
        pass

    def dummyChallengesPassed():
        return set()

    def dummyChallenges():
        return [
            {
                "type": "options",
                "formInTargetLanguage": "La médica bebe agua",
                "meaningInSourceLanguage": "The doctor drinks water",
                "id": "c4e5f63786b6",
                "priority": 0,
                "group": "89a5e1d657d0",
            },
            {
                "type": "chips",
                "translatesToSourceLanguage": False,
                "phrase": [
                    {"word": "The", "definition": "el\nla"},
                    {"word": "doctor", "definition": "médico"},
                    {"word": "drinks", "definition": "bebe\nbebidas"},
                    {"word": "water", "definition": "agua"},
                ],
                "chips": ["La", "médica", "bebe", "agua", "Agua", "a", "la"],
                "solutions": [
                    ["La", "médica", "bebe", "agua"],
                    ["La", "médica", "bebe", "agua"],
                ],
                "formattedSolution": "La médica bebe agua",
                "id": "3cee9492930e",
                "priority": 2,
                "group": "89a5e1d657d0",
            },
            {
                "type": "chips",
                "translatesToSourceLanguage": True,
                "phrase": [
                    {"word": "La", "definition": "the"},
                    {"word": "médica", "definition": "doctor"},
                    {"word": "bebe", "definition": "drinks"},
                    {"word": "agua", "definition": "water"},
                ],
                "chips": [
                    "The",
                    "doctor",
                    "drinks",
                    "water",
                    "Water",
                    "waiter",
                    "later",
                ],
                "solutions": [["The", "doctor", "drinks", "water"]],
                "formattedSolution": "The doctor drinks water",
                "id": "3df1829df970",
                "priority": 2,
                "group": "89a5e1d657d0",
            },
            {
                "type": "options",
                "formInTargetLanguage": "El camarero trabaja",
                "meaningInSourceLanguage": "The waiter works",
                "id": "09d0b0b28df7",
                "priority": 0,
                "group": "44b523014508",
            },
            {
                "type": "chips",
                "translatesToSourceLanguage": False,
                "phrase": [
                    {"word": "The", "definition": "el\nla"},
                    {"word": "waiter", "definition": "camarero"},
                    {"word": "works", "definition": "funciona\ntrabaja"},
                ],
                "chips": ["El", "camarero", "trabaja", "tal", "bajo"],
                "solutions": [
                    ["El", "camarero", "trabaja"],
                    ["La", "camarera", "trabaja"],
                ],
                "formattedSolution": "El camarero trabaja",
                "id": "dbfb43e41620",
                "priority": 2,
                "group": "44b523014508",
            },
            {
                "type": "chips",
                "translatesToSourceLanguage": True,
                "phrase": [
                    {"word": "El", "definition": "the"},
                    {"word": "camarero", "definition": "waiter"},
                    {"word": "trabaja", "definition": "works"},
                ],
                "chips": ["The", "waiter", "works", "are", "work"],
                "solutions": [["The", "waiter", "works"]],
                "formattedSolution": "The waiter works",
                "id": "f4255f18a924",
                "priority": 2,
                "group": "44b523014508",
            },
            {
                "type": "options",
                "formInTargetLanguage": "Las bomberas llegan",
                "meaningInSourceLanguage": "The firefighters arrive",
                "id": "1b2e6dea7b20",
                "priority": 0,
                "group": "9f034f444f71",
            },
            {
                "type": "chips",
                "translatesToSourceLanguage": False,
                "phrase": [
                    {"word": "The", "definition": "el\nla"},
                    {"word": "firefighters", "definition": "bomberas\nbomberos"},
                    {"word": "arrive", "definition": "llegan\nllegas\nllego"},
                ],
                "chips": ["Las", "bomberas", "llegan", "can", "Pan"],
                "solutions": [
                    ["Las", "bomberas", "llegan"],
                    ["Los", "bomberos", "llegan"],
                ],
                "formattedSolution": "Las bomberas llegan",
                "id": "2b607289e390",
                "priority": 2,
                "group": "9f034f444f71",
            },
            {
                "type": "chips",
                "translatesToSourceLanguage": True,
                "phrase": [
                    {"word": "Las", "definition": "the"},
                    {"word": "bomberas", "definition": "firefighters"},
                    {"word": "llegan", "definition": "arrive"},
                ],
                "chips": ["The", "firefighters", "arrive", "are", "the"],
                "solutions": [["The", "firefighters", "arrive"]],
                "formattedSolution": "The firefighters arrive",
                "id": "29b578bf6d76",
                "priority": 2,
                "group": "9f034f444f71",
            },
            {
                "type": "options",
                "formInTargetLanguage": "El granjero ve los animales",
                "meaningInSourceLanguage": "The farmer sees the animals",
                "id": "bf0e35a2745d",
                "priority": 0,
                "group": "ed61a410a2fc",
            },
            {
                "type": "chips",
                "translatesToSourceLanguage": False,
                "phrase": [
                    {"word": "The", "definition": "el\nla"},
                    {"word": "farmer", "definition": "granjera\ngranjero"},
                    {"word": "sees", "definition": "ve"},
                    {"word": "the", "definition": "el\nla"},
                    {"word": "animals", "definition": "animales"},
                ],
                "chips": [
                    "El",
                    "granjero",
                    "ve",
                    "los",
                    "animales",
                    "no",
                    "al",
                    "Es",
                    "Me",
                ],
                "solutions": [
                    ["El", "granjero", "ve", "los", "animales"],
                    ["La", "granjera", "ve", "los", "animales"],
                ],
                "formattedSolution": "El granjero ve los animales",
                "id": "fc2e93c792c5",
                "priority": 2,
                "group": "ed61a410a2fc",
            },
            {
                "type": "chips",
                "translatesToSourceLanguage": True,
                "phrase": [
                    {"word": "El", "definition": "the"},
                    {"word": "granjero", "definition": "farmer"},
                    {"word": "ve", "definition": "sees"},
                    {"word": "los", "definition": "the"},
                    {"word": "animales", "definition": "animals"},
                ],
                "chips": [
                    "The",
                    "farmer",
                    "sees",
                    "the",
                    "animals",
                    "are",
                    "She",
                    "They",
                    "name",
                ],
                "solutions": [["The", "farmer", "sees", "the", "animals"]],
                "formattedSolution": "The farmer sees the animals",
                "id": "45e1bd004744",
                "priority": 2,
                "group": "ed61a410a2fc",
            },
            {
                "type": "options",
                "formInTargetLanguage": "Los enfermeros viven aquí",
                "meaningInSourceLanguage": "The nurses live here",
                "id": "460a28a8781c",
                "priority": 0,
                "group": "03d6b8783292",
            },
            {
                "type": "chips",
                "translatesToSourceLanguage": False,
                "phrase": [
                    {"word": "The", "definition": "el\nla"},
                    {"word": "nurses", "definition": "enfermeras\nenfermeros"},
                    {"word": "live", "definition": "vive\nviven\nvivo"},
                    {"word": "here", "definition": "aquí"},
                ],
                "chips": ["Los", "enfermeros", "viven", "aquí", "eres", "los", "en"],
                "solutions": [
                    ["Los", "enfermeros", "viven", "aquí"],
                    ["Las", "enfermeras", "viven", "aquí"],
                ],
                "formattedSolution": "Los enfermeros viven aquí",
                "id": "ec6f70f09253",
                "priority": 2,
                "group": "03d6b8783292",
            },
            {
                "type": "chips",
                "translatesToSourceLanguage": True,
                "phrase": [
                    {"word": "Los", "definition": "the"},
                    {"word": "enfermeros", "definition": "nurses"},
                    {"word": "viven", "definition": "live"},
                    {"word": "aquí", "definition": "here"},
                ],
                "chips": ["The", "nurses", "live", "here", "are", "like", "the"],
                "solutions": [["The", "nurses", "live", "here"]],
                "formattedSolution": "The nurses live here",
                "id": "2726a8ed6ba8",
                "priority": 2,
                "group": "03d6b8783292",
            },
            {
                "type": "options",
                "formInTargetLanguage": "El cantante canta bien",
                "meaningInSourceLanguage": "The singer sings well",
                "id": "51a94eac6f08",
                "priority": 0,
                "group": "1ccc93474057",
            },
            {
                "type": "chips",
                "translatesToSourceLanguage": False,
                "phrase": [
                    {"word": "The", "definition": "el\nla"},
                    {"word": "singer", "definition": "cantante"},
                    {"word": "sings", "definition": "canta"},
                    {"word": "well", "definition": "bien"},
                ],
                "chips": ["El", "cantante", "canta", "bien", "can", "alta", "cena"],
                "solutions": [
                    ["El", "cantante", "canta", "bien"],
                    ["El", "cantor", "canta", "bien"],
                    ["La", "cantora", "canta", "bien"],
                ],
                "formattedSolution": "El cantante canta bien",
                "id": "190a4bcac181",
                "priority": 2,
                "group": "1ccc93474057",
            },
            {
                "type": "chips",
                "translatesToSourceLanguage": True,
                "phrase": [
                    {"word": "El", "definition": "the"},
                    {"word": "cantante", "definition": "singer"},
                    {"word": "canta", "definition": "sings"},
                    {"word": "bien", "definition": "well"},
                ],
                "chips": ["The", "singer", "sings", "well", "wine", "sees", "song"],
                "solutions": [["The", "singer", "sings", "well"]],
                "formattedSolution": "The singer sings well",
                "id": "cf875283c3d1",
                "priority": 2,
                "group": "1ccc93474057",
            },
            {
                "type": "cards",
                "pictures": ["doctor1.jpg", "doctor2.jpg", "doctor3.jpg"],
                "formInTargetLanguage": "médico",
                "meaningInSourceLanguage": "doctor",
                "id": "e79d95d967ad",
                "priority": 0,
                "group": "d4277f704435",
            },
            {
                "type": "shortInput",
                "pictures": ["doctor1.jpg", "doctor2.jpg", "doctor3.jpg"],
                "formInTargetLanguage": ["médico", "médica"],
                "phrase": [{"word": "doctor", "definition": "médico"}],
                "id": "7f5dd6ae07fb",
                "priority": 1,
                "group": "d4277f704435",
            },
            {
                "type": "cards",
                "pictures": [
                    "firefighter1.jpg",
                    "firefighter2.jpg",
                    "firefighter3.jpg",
                ],
                "formInTargetLanguage": "bombera",
                "meaningInSourceLanguage": "firefighter",
                "id": "ebecf8819ee2",
                "priority": 0,
                "group": "776f238f0571",
            },
            {
                "type": "shortInput",
                "pictures": [
                    "firefighter1.jpg",
                    "firefighter2.jpg",
                    "firefighter3.jpg",
                ],
                "formInTargetLanguage": ["bombera", "bombero"],
                "phrase": [{"word": "firefighter", "definition": "bombera\nbombero"}],
                "id": "93c8848658ca",
                "priority": 1,
                "group": "776f238f0571",
            },
            {
                "type": "cards",
                "pictures": [
                    "firefighter1.jpg",
                    "firefighter2.jpg",
                    "firefighter3.jpg",
                ],
                "formInTargetLanguage": "bombero",
                "meaningInSourceLanguage": "firefighter",
                "id": "3d5fc397ec0a",
                "priority": 0,
                "group": "de16f62c2e4f",
            },
            {
                "type": "shortInput",
                "pictures": [
                    "firefighter1.jpg",
                    "firefighter2.jpg",
                    "firefighter3.jpg",
                ],
                "formInTargetLanguage": ["bombero", "bombera"],
                "phrase": [{"word": "firefighter", "definition": "bombera\nbombero"}],
                "id": "4d9c5a4677d4",
                "priority": 1,
                "group": "de16f62c2e4f",
            },
            {
                "type": "cards",
                "pictures": ["farmer1.jpg", "farmer2.jpg", "farmer3.jpg"],
                "formInTargetLanguage": "granjera",
                "meaningInSourceLanguage": "farmer",
                "id": "e30c6056f7a3",
                "priority": 0,
                "group": "524e85ff33bc",
            },
            {
                "type": "shortInput",
                "pictures": ["farmer1.jpg", "farmer2.jpg", "farmer3.jpg"],
                "formInTargetLanguage": ["granjera", "granjero"],
                "phrase": [{"word": "farmer", "definition": "granjera\ngranjero"}],
                "id": "0e3c3da64e84",
                "priority": 1,
                "group": "524e85ff33bc",
            },
            {
                "type": "cards",
                "pictures": ["farmer1.jpg", "farmer2.jpg", "farmer3.jpg"],
                "formInTargetLanguage": "granjero",
                "meaningInSourceLanguage": "farmer",
                "id": "fe1c7a9db989",
                "priority": 0,
                "group": "08a90c3722b2",
            },
            {
                "type": "shortInput",
                "pictures": ["farmer1.jpg", "farmer2.jpg", "farmer3.jpg"],
                "formInTargetLanguage": ["granjero", "granjera"],
                "phrase": [{"word": "farmer", "definition": "granjera\ngranjero"}],
                "id": "865e1d746c1a",
                "priority": 1,
                "group": "08a90c3722b2",
            },
            {
                "type": "cards",
                "pictures": ["doctor1.jpg", "doctor2.jpg", "doctor3.jpg"],
                "formInTargetLanguage": "una médica",
                "meaningInSourceLanguage": "a doctor",
                "id": "2fc0d11539b2",
                "priority": 0,
                "group": "3c7bd404c230",
            },
            {
                "type": "shortInput",
                "pictures": ["doctor1.jpg", "doctor2.jpg", "doctor3.jpg"],
                "formInTargetLanguage": ["una médica", "un médico"],
                "phrase": [
                    {"word": "a", "definition": "un\nuna"},
                    {"word": "doctor", "definition": "médico"},
                ],
                "id": "c73d1de3180a",
                "priority": 1,
                "group": "3c7bd404c230",
            },
            {
                "type": "cards",
                "pictures": ["farmer1.jpg", "farmer2.jpg", "farmer3.jpg"],
                "formInTargetLanguage": "granjeras",
                "meaningInSourceLanguage": "farmers",
                "id": "5bc16332e32e",
                "priority": 0,
                "group": "7b3371aa8c1f",
            },
            {
                "type": "shortInput",
                "pictures": ["farmer1.jpg", "farmer2.jpg", "farmer3.jpg"],
                "formInTargetLanguage": ["granjeras", "granjeros"],
                "phrase": [{"word": "farmers", "definition": "granjeras"}],
                "id": "7e6778992c23",
                "priority": 1,
                "group": "7b3371aa8c1f",
            },
        ]

    progress_calculator.loaded = True
    progress_calculator.challengesPassed = dummyChallengesPassed
    challenge_calculator.validChallenges = dummyChallenges

    dummy_skill_data = SkillData(
        skill_id="id1",
        name="Basics",
        words=[
            Word(
                in_target_language=["médico", "médica"],
                in_source_language=["doctor"],
                pictures=["doctor1", "doctor2", "doctor3"],
            ),
            Word(
                in_target_language=["bombera", "bombero"],
                in_source_language=["firefighter"],
                pictures=["firefighter1", "firefighter2", "firefighter3"],
            ),
            Word(
                in_target_language=["bombero", "bombera"],
                in_source_language=["firefighter"],
                pictures=["firefighter1", "firefighter2", "firefighter3"],
            ),
            Word(
                in_target_language=["granjera", "granjero"],
                in_source_language=["farmer"],
                pictures=["farmer1", "farmer2", "farmer3"],
            ),
            Word(
                in_target_language=["granjero", "granjera"],
                in_source_language=["farmer"],
                pictures=["farmer1", "farmer2", "farmer3"],
            ),
            Word(
                in_target_language=["una médica", "un médico"],
                in_source_language=["a doctor"],
                pictures=["doctor1", "doctor2", "doctor3"],
            ),
            Word(
                in_target_language=["granjeras", "granjeros"],
                in_source_language=["farmers"],
                pictures=["farmer1", "farmer2", "farmer3"],
            ),
        ],
        phrases=[
            Phrase(
                in_target_language=["La médica bebe agua", "La médica bebe agua"],
                in_source_language=["The doctor drinks water"],
            ),
            Phrase(
                in_target_language=["El camarero trabaja", "La camarera trabaja"],
                in_source_language=["The waiter works"],
            ),
            Phrase(
                in_target_language=["Las bomberas llegan", "Los bomberos llegan"],
                in_source_language=["The firefighters arrive"],
            ),
            Phrase(
                in_target_language=[
                    "El granjero ve los animales",
                    "La  granjera ve los animales",
                ],
                in_source_language=["The farmer sees the animals"],
            ),
            Phrase(
                in_target_language=[
                    "Los enfermeros viven aquí",
                    "Las enfermeras viven aquí",
                ],
                in_source_language=["The nurses live here"],
            ),
            Phrase(
                in_target_language=[
                    "El cantante canta bien",
                    "El cantor canta bien",
                    "La cantora canta bien",
                ],
                in_source_language=["The singer sings well"],
            ),
        ],
        course_name="spanish-from-english",
    )

    language_skill = LanguageSkill(challenge_calculator, progress_calculator)
    language_skill._skill_data = dummy_skill_data
    language_skill._levels = 3
    language_skill.validateLoad = dummyValidateLoad
    language_skill.loaded = True

    return language_skill


@pytest.fixture(scope="function", name="flow_with_loaded_challenge")
def fixture_flow_with_loaded_challenge(valid_model, validator):
    progress_calculator = ProgressCalculator(None)
    flow = PracticeSessionFlow(validator, None, progress_calculator)
    flow.loadSessionChallenges(valid_model, False)
    flow.loadNextChallenge()

    return flow


@pytest.fixture(scope="function", name="flow_with_loaded_already_passed_challenge")
def fixture_flow_with_loaded_already_passed_challenge(valid_model, validator):
    progress_calculator = ProgressCalculator(None)
    flow = PracticeSessionFlow(validator, None, progress_calculator)
    flow.loadSessionChallenges(valid_model, False)
    flow.loadNextChallenge()
    loaded_challenge_id = flow._session_state.current_challenge.data["id"]

    def dummyPassed():
        return {loaded_challenge_id}

    flow._language_skill._progress_calculator.challengesPassed = dummyPassed

    return flow


def test_loadSessionChallenges_inp_valid_language_skill_out_returns_true(valid_model):
    flow = PracticeSessionFlow(None, None, None)

    assert flow.loadSessionChallenges(valid_model, False)


def test_SessionState_inp_loadedSessionChallenges_out_challenges_populated(valid_model):
    flow = PracticeSessionFlow(None, None, None)
    flow.loadSessionChallenges(valid_model, False)

    assert flow._session_state.challenges


def test_SessionState_inp_loadedSessionChallenges_out_challenges_amt_populated(
    valid_model,
):
    flow = PracticeSessionFlow(None, None, None)
    flow.loadSessionChallenges(valid_model, False)

    assert flow._session_state.challenges_amt > 0


def test_SessionState_inp_loadedSessionChallenges_out_empty_passed_challenges(
    valid_model,
):
    flow = PracticeSessionFlow(None, None, None)
    flow.loadSessionChallenges(valid_model, False)

    assert not flow._session_state.passed_challenges


def test_SessionState_inp_loadedSessionChallenges_out_empty_challenges_to_repeat(
    valid_model,
):
    flow = PracticeSessionFlow(None, None, None)
    flow.loadSessionChallenges(valid_model, False)

    assert not flow._session_state.challenges_to_repeat


def test_SessionState_inp_loadedSessionChallenges_out_empty_stats(valid_model):
    flow = PracticeSessionFlow(None, None, None)
    flow.loadSessionChallenges(valid_model, False)

    assert (
        (flow._session_state.stats.correct == 0)
        and (flow._session_state.stats.incorrect == 0)
        and (flow._session_state.stats.expected > 0)
    )


def test_loadNextChallenge_inp_loadedSessionChallenges_out_true(valid_model, validator):
    flow = PracticeSessionFlow(validator, None, None)
    flow.loadSessionChallenges(valid_model, False)

    assert flow.loadNextChallenge()


def test_SessionState_inp_loadedNextChallenge_out_current_challenge_data_populated(
    flow_with_loaded_challenge,
):
    assert flow_with_loaded_challenge._session_state.current_challenge.data


def test_SessionState_inp_loadedNextChallenge_out_current_challenge_question_populated_type(
    flow_with_loaded_challenge,
):
    assert (
        flow_with_loaded_challenge._session_state.current_challenge.question.challengeType
    )


def test_SessionState_inp_loadedNextChallenge_out_current_challenge_question_populated_cid(
    flow_with_loaded_challenge,
):
    assert flow_with_loaded_challenge._session_state.current_challenge.question.cid


def test_SessionState_inp_loadedNextChallenge_out_current_challenge_question_populated_group(
    flow_with_loaded_challenge,
):
    assert flow_with_loaded_challenge._session_state.current_challenge.question.group


def test_SessionState_inp_loadedNextChallenge_out_current_challenge_answer_populated_cid(
    flow_with_loaded_challenge,
):
    assert flow_with_loaded_challenge._session_state.current_challenge.answer.cid


def test_SessionState_inp_loadedNextChallenge_out_cc_answer_populated_formInTargetLanguage(
    flow_with_loaded_challenge,
):

    assert (
        flow_with_loaded_challenge._session_state.current_challenge.answer.formInTargetLanguage
    )


def test_loadNextChallenge_inp_loop_through_all_challenges_out_all_return_true(
    valid_model, validator
):
    flow = PracticeSessionFlow(validator, None, None)
    flow.loadSessionChallenges(valid_model, False)

    for _ in range(flow._session_state.challenges_amt):
        assert flow.loadNextChallenge()


def test_loadNextChallenge_inp_loop_through_all_challenges_out_last_load_returns_false(
    valid_model, validator
):

    flow = PracticeSessionFlow(validator, None, None)
    flow.loadSessionChallenges(valid_model, False)

    for _ in range(flow._session_state.challenges_amt):
        flow.loadNextChallenge()

    assert not flow.loadNextChallenge()


def test_checkResult_inp_flow_with_loaded_challenge_and_correct_answer_given_out_returns_true(
    flow_with_loaded_challenge,
):
    flow_with_loaded_challenge._checkAnswer = lambda x: True

    assert flow_with_loaded_challenge.checkResult({})


def test_SessionState_inp_flow_with_loaded_challenge_n_correct_answer_out_stats_updated(
    flow_with_loaded_challenge,
):
    flow_with_loaded_challenge._checkAnswer = lambda x: True
    flow_with_loaded_challenge.checkResult({})

    assert flow_with_loaded_challenge._session_state.stats.correct == 1


def test_SessionState_inp_flow_with_loaded_challenge_n_correct_answer_out_passed_updated(
    flow_with_loaded_challenge,
):
    flow_with_loaded_challenge._checkAnswer = lambda x: True
    flow_with_loaded_challenge.checkResult({})

    assert len(flow_with_loaded_challenge._session_state.passed_challenges) == 1


def test_SessionState_inp_flow_with_loaded_challenge_n_correct_answer_for_already_passed_out_passed_not_updated(
    flow_with_loaded_already_passed_challenge,
):
    flow_with_loaded_already_passed_challenge._checkAnswer = lambda x: True
    flow_with_loaded_already_passed_challenge.checkResult({})

    assert (
        len(flow_with_loaded_already_passed_challenge._session_state.passed_challenges)
        == 0
    )


def test_checkResult_inp_flow_with_loaded_challenge_and_wrong_answer_given_out_returns_false(
    flow_with_loaded_challenge,
):
    flow_with_loaded_challenge._checkAnswer = lambda x: False

    assert not flow_with_loaded_challenge.checkResult({})


def test_SessionState_inp_flow_with_loaded_challenge_n_wrong_answer_out_stats_updated(
    flow_with_loaded_challenge,
):
    flow_with_loaded_challenge._checkAnswer = lambda x: False
    flow_with_loaded_challenge.checkResult({})

    assert flow_with_loaded_challenge._session_state.stats.incorrect == 1


def test_SessionState_inp_flow_with_loaded_challenge_n_wrong_answer_out_added_for_repeat(
    flow_with_loaded_challenge,
):
    flow_with_loaded_challenge._checkAnswer = lambda x: False
    flow_with_loaded_challenge.checkResult({})

    assert len(flow_with_loaded_challenge._session_state.challenges_to_repeat) == 1


def test_SessionState_inp_flow_with_loaded_challenge_n_wrong_answer_out_added_for_repeat_with_valid_id(
    flow_with_loaded_challenge,
):
    flow_with_loaded_challenge._checkAnswer = lambda x: False
    flow_with_loaded_challenge.checkResult({})

    assert flow_with_loaded_challenge._session_state.challenges_to_repeat[0]["id"]
