# SPDX-FileCopyrightText: 2022 Dimitris Kardarakos <dimkard@posteo.net>
# SPDX-License-Identifier: GPL-3.0-or-later

from datetime import datetime
from sqlalchemy import select
from sqlalchemy.sql import text
from sqlalchemy.orm import Session, clear_mappers
from sqlalchemy import inspect
from glossaico.alchemy_database import AlchemyDatabase
from glossaico.practice_session_data import PracticeSessionData, PassedChallenges


def test_mapping_inp_valid_model_out_is_mapped():
    clear_mappers()
    db_loader = AlchemyDatabase("sqlite+pysqlite:///:memory:")
    db_loader.load()
    mapped = inspect(PracticeSessionData)

    assert mapped


def test_load_inp_valid_table_data_out_data_added_to_table():
    clear_mappers()
    db_loader = AlchemyDatabase("sqlite+pysqlite:///:memory:")
    engine = db_loader.load()
    db_session = Session(engine)
    db_session.execute(
        text(
            "INSERT INTO practice_session (course, correct_challenges, practice_dt, expected_challenges, skill_id) VALUES(:crs, :cchl, :dt, :exchl, :skl)"
        ),
        {
            "crs": "dummy-course",
            "cchl": 20,
            "dt": datetime.now(),
            "exchl": 30,
            "skl": "dummy_skill",
        },
    )

    db_session.commit()
    result = db_session.execute(select(PracticeSessionData))

    assert result


def test_load_inp_new_model_data_out_added_to_table():
    clear_mappers()
    db_loader = AlchemyDatabase("sqlite+pysqlite:///:memory:")
    engine = db_loader.load()

    passed_a = {PassedChallenges("ch1"), PassedChallenges("ch2")}
    passed_b = {PassedChallenges("ch3"), PassedChallenges("ch4")}

    practice_data = PracticeSessionData(
        "dummy-course", datetime.now(), 12, 15, "skill_1", passed_a
    )
    practice_data2 = PracticeSessionData(
        "dummy-course", datetime.now(), 3, 14, "skill_2", passed_b
    )

    db_session = Session(engine)
    db_session.add(practice_data)
    db_session.add(practice_data2)
    db_session.flush()
    db_session.commit()

    result = db_session.execute(
        select(PracticeSessionData).filter_by(skill_id="skill_2")
    ).scalar_one()

    assert result.skill_id == "skill_2"
