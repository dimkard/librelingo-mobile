# SPDX-FileCopyrightText: 2022 Dimitris Kardarakos <dimkard@posteo.net>
# SPDX-License-Identifier: GPL-3.0-or-later


from pathlib import Path
import pytest
from glossaico.course_validation import CourseValidation


@pytest.fixture(scope="function", name="dummy_validator")
def fixture_dummy_validator(tmp_path_factory):
    images = tmp_path_factory.mktemp("images")
    voices = tmp_path_factory.mktemp("voices")

    return CourseValidation(images.resolve(), voices.resolve())


def test_imageExists_inp_existing_image_out_true(dummy_validator):
    name = "test.jpg"
    dummy_file_path = Path(f"{dummy_validator._image_dir}/{name}")

    with open(dummy_file_path, "w", encoding="utf-8") as f:
        f.write("")

    assert dummy_validator.imageExists(name)


def test_imageExists_inp_non_existing_image_out_false(dummy_validator):
    assert not dummy_validator.imageExists("non-existing-image")


def test_voiceExists_inp_existing_file_out_true(dummy_validator):
    name = "test"
    dummy_file_path = Path(f"{dummy_validator._voice_dir}/{name}.mp3")

    with open(dummy_file_path, "w", encoding="utf-8") as f:
        f.write("")

    assert dummy_validator.voiceExists(name)


def test_voiceExists_inp_non_existing_file_out_false(dummy_validator):
    assert not dummy_validator.voiceExists("non-existing-voice")
