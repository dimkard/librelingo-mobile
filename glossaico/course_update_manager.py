# SPDX-FileCopyrightText: 2023 Dimitris Kardarakos <dimkard@posteo.net>
# SPDX-License-Identifier: GPL-3.0-or-later

from pathlib import Path
import hashlib
import zipfile
import os
from shutil import rmtree
from functools import lru_cache
import logging
import requests
from yaml import safe_load


class CourseUpdateManager:
    """Class that handles course data updates"""

    update_type_codes = {"none": 0, "insert": 1, "update": 2}
    remote_config_url = "https://codeberg.org/dimkard/glossaico-data-config/raw/branch/main/configuration.yaml"
    course_data_structure = {
        "container": "glossaico-data",
        "voices_dir": "voice",
        "images_dir": "images",
        "courses_dir": "courses",
        "courses_file": "courses.json",
    }

    def __init__(self, app_data_dir: str, cache_dir: str, installed_data_checksum: str):
        self._app_data_dir = app_data_dir
        self._cache_dir = cache_dir
        self._installed_data_checksum = installed_data_checksum
        self._active_download_name = ""

    def downloadFile(self, download_name: str, remote_url: str, file_path: str):
        """Downloads a file from a remote url and saves it as a local file"""
        self._active_download_name = download_name
        req = requests.get(remote_url, timeout=300)

        if req.status_code != 200:
            raise requests.HTTPError(
                f"""
            Cannot download from {remote_url}: {req.status_code}, {req.reason}
            """
            )
        with open(file_path, "wb") as file:
            file.write(req.content)

    @staticmethod
    def _extractDataArchive(archive_file: str, target_dir: str) -> str:
        container_dir = CourseUpdateManager.course_data_structure["container"]
        courses_dir = CourseUpdateManager.course_data_structure["courses_dir"]
        images_dir = CourseUpdateManager.course_data_structure["images_dir"]
        voices_dir = CourseUpdateManager.course_data_structure["voices_dir"]
        courses_file = CourseUpdateManager.course_data_structure["courses_file"]

        with zipfile.ZipFile(archive_file, "r") as zip_obj:
            for member_name in zip_obj.namelist():
                if (
                    member_name.startswith(f"{container_dir}/{courses_dir}")
                    or member_name.startswith(f"{container_dir}/{images_dir}")
                    or member_name.startswith(f"{container_dir}/{voices_dir}")
                    or member_name.startswith(f"{container_dir}/{courses_file}")
                ):
                    zip_obj.extract(member_name, target_dir)

        return f"{target_dir}/{container_dir}"

    def _fetchRemoteArchive(self, remote_url: str, archive_checksum: str) -> str:
        new_data_file_path = f"{self._cache_dir}/glossaico_data.zip"
        self.downloadFile("glossaico_data", remote_url, new_data_file_path)

        if self.__class__.digestSha256Sum(new_data_file_path) != archive_checksum:
            raise Exception("The checksum of the downloaded file is invalid.")

        return new_data_file_path

    @staticmethod
    def digestSha256Sum(file_path: str):
        """Returns the SHA 256 sum of a file"""
        check_sum = "_"
        with open(file_path, "rb") as f:
            bytes_read = f.read()
            check_sum = hashlib.sha256(bytes_read).hexdigest()

        return check_sum

    @staticmethod
    def _createFreshDir(dir_path_str: str) -> str:
        """Deletes a directory if exists and creates a new empty one.
        Returns its path"""

        dir_path = Path(dir_path_str)

        if dir_path.is_dir():
            rmtree(dir_path)

        dir_path.mkdir()

        return dir_path_str

    @staticmethod
    def updateCourseDirs(src: str, dst: str):
        """Move extracted directories to the target data directory"""
        src_dirs = [
            f"{src}/{CourseUpdateManager.course_data_structure['voices_dir']}",
            f"{src}/{CourseUpdateManager.course_data_structure['images_dir']}",
            f"{src}/{CourseUpdateManager.course_data_structure['courses_dir']}",
        ]

        for d in src_dirs:
            if not Path(d).is_dir():
                raise NotADirectoryError(f"{d} expected, not found")

        src_courses_file = (
            f"{src}/{CourseUpdateManager.course_data_structure['courses_file']}"
        )

        if not Path(src_courses_file).is_file():
            raise FileNotFoundError(f"{src_courses_file} expected, not found")

        dst_dirs = [
            f"{dst}/{CourseUpdateManager.course_data_structure['voices_dir']}",
            f"{dst}/{CourseUpdateManager.course_data_structure['images_dir']}",
            f"{dst}/{CourseUpdateManager.course_data_structure['courses_dir']}",
        ]

        dst_courses_file = (
            f"{dst}/{CourseUpdateManager.course_data_structure['courses_file']}"
        )

        for d in dst_dirs:
            d_path = Path(d)
            if d_path.is_dir():
                rmtree(d_path)

        for i, src_dir in enumerate(src_dirs):
            os.rename(src_dir, dst_dirs[i])

        os.replace(src_courses_file, dst_courses_file)

    def updateCourseContent(self) -> str:
        """Manages the download of the course, voice and image data
        needed by the application. Returns the checksum of the updated course data"""

        try:
            self._active_download_name = ""

            remote_url: str = self.remoteDataArchiveUrl()
            archive_checksum: str = self.remoteDataArchiveChecksum()

            cache_tmp_dir = self._createFreshDir(
                f"{self._cache_dir}/glossaico_data_{archive_checksum}"
            )

            new_data_file: str = self._fetchRemoteArchive(remote_url, archive_checksum)
            extacted_data_dir = self._extractDataArchive(new_data_file, cache_tmp_dir)

            CourseUpdateManager.updateCourseDirs(extacted_data_dir, self._app_data_dir)

            return archive_checksum
        except requests.RequestException as exc:
            raise RuntimeError(
                "No data has been updated, please check your network connectivity"
            ) from exc

    @lru_cache(maxsize=1)
    def remoteDataConfiguration(self):
        """Returns the sha256 checksum and the download URL of the latest
        glossaico data release archive"""
        remote_config = {}
        remote_config_file_name = "remote-configuration.yaml"

        remote_config_save_path = Path(self._cache_dir, remote_config_file_name)
        try:
            self.downloadFile(
                remote_config_file_name,
                self.__class__.remote_config_url,
                remote_config_save_path,
            )
            with open(remote_config_save_path, "r", encoding="utf-8") as cfg:
                remote_config = safe_load(cfg)

        except requests.RequestException:
            logging.error("Network error while fetching remote course configuration")
        except OSError:
            logging.error("Error while saving remote configuration file")

        return remote_config

    def dataUpdateType(self) -> int:
        """Returns:
        - none code, if no course content update is available
        - insert code, if it is the first time course content will be added
        - update code, if course content exists and it can be updated
        """
        local = self._installed_data_checksum
        remote = self.remoteDataArchiveChecksum()

        if remote and local and (remote != local):
            return self.__class__.update_type_codes["update"]

        if remote and local and (remote == local):
            return self.__class__.update_type_codes["none"]

        if remote and (not local):
            return self.__class__.update_type_codes["insert"]

        if (not remote) and local:
            return self.__class__.update_type_codes["none"]

        if (not remote) and (not local):
            raise RuntimeError(
                """
                    Course content cannot be installed.
                    Please check your network connection"""
            )

        raise RuntimeError("Update type cannot be defined")

    def remoteDataArchiveChecksum(self) -> str:
        """Returns the sha256 sum of the remote course content archive"""
        remote_data_cfg = self.remoteDataConfiguration()
        if ("glossaicoData" in remote_data_cfg) and (
            "sha256sum" in remote_data_cfg["glossaicoData"]
        ):
            return remote_data_cfg["glossaicoData"]["sha256sum"]

        return ""

    def remoteDataArchiveUrl(self) -> str:
        """Returns the url to fetch the remote course content archive from"""
        remote_data_cfg = self.remoteDataConfiguration()
        if ("glossaicoData" in remote_data_cfg) and (
            "url" in remote_data_cfg["glossaicoData"]
        ):
            return remote_data_cfg["glossaicoData"]["url"]

        return ""

    def canUpdate(self) -> bool:
        """Returns True if there is updated course content available.
        Otherwise, it returns False"""

        upd_type = self.dataUpdateType()
        if upd_type == self.__class__.update_type_codes["none"]:
            return False

        if upd_type in (
            self.__class__.update_type_codes["insert"],
            self.__class__.update_type_codes["update"],
        ):
            return True

        raise RuntimeError("Cannot define if data should be updated")
