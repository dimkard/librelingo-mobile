/*
 * SPDX-FileCopyrightText: 2023 Dimitris Kardarakos <dimkard@posteo.net>
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

import QtQuick 2.12
import QtQuick.Controls 2.12 as Controls
import QtQuick.Layouts 1.12
import org.kde.kirigami 2.12 as Kirigami
import org.codeberg.dimkard.glossaico 1.0 as Glossaico

Kirigami.ScrollablePage {
    id: root

    property string skillName
    property string skillWordsAndPhrases
    property string introduction

    signal sessionStart
    signal sessionEnd

    title: skillName

    footer: MessageFooter {
        id: messageFooter

        rightActions: [
            Kirigami.Action {
                text: QGlTr._("Back to Skills")

                onTriggered: root.sessionEnd()
            },

            Kirigami.Action {
                text: QGlTr._("Practice %1").arg(root.skillName)

                onTriggered: root.sessionStart()
            }
        ]
    }

    ColumnLayout {
        anchors {
            left: parent.left
            right: parent.right
            top: parent.top
        }
        spacing: Kirigami.Units.largeSpacing

        Kirigami.Heading {
            visible: !root.introduction
            text: root.skillName
            wrapMode: Text.WordWrap

            Layout.fillWidth: true
        }

        Controls.Label {
            visible: !root.introduction && root.skillWordsAndPhrases
            text: QGlTr._("Learn: %1").arg(root.skillWordsAndPhrases)
            wrapMode: Text.WordWrap

            Layout.fillWidth: true
        }

        Controls.Label {
            visible: root.introduction
            text: root.introduction
            textFormat: Text.RichText
            wrapMode: Text.WordWrap

            Layout.fillWidth: true
        }
    }
}
