# SPDX-FileCopyrightText: 2022 Dimitris Kardarakos <dimkard@posteo.net>
# SPDX-License-Identifier: GPL-3.0-or-later


from pathlib import Path
import pytest
from glossaico.directory_settings import DirectorySettings


@pytest.fixture(scope="function", name="dummy_dir_settings")
def fixture_dummy_dir_settings(tmp_path_factory):
    """Provide an dir settings object without loading it"""
    data_loc = tmp_path_factory.mktemp("share_data")
    cache_loc = tmp_path_factory.mktemp("cache")
    dir_settings = DirectorySettings(data_loc, cache_loc, "glossaico")

    return dir_settings


def test_soundDir_inp_valid_dummy_dir_settings_out_directory_exists(dummy_dir_settings):
    assert Path(dummy_dir_settings.soundDir).is_dir()


def test_iconDir_inp_valid_dummy_dir_settings_out_directory_exists(dummy_dir_settings):
    assert Path(dummy_dir_settings.iconDir).is_dir()


def test_appDataDir_inp_valid_dummy_dir_settings_out_directory_exists(
    dummy_dir_settings,
):

    assert Path(dummy_dir_settings.appDataDir).is_dir()


def test_imageDir_inp_valid_dummy_dir_settings_out_directory_exists(dummy_dir_settings):
    assert Path(dummy_dir_settings.imageDir).is_dir()


def test_courseDir_inp_valid_dummy_dir_settings_out_directory_exists(
    dummy_dir_settings,
):
    assert Path(dummy_dir_settings.courseDir).is_dir()


def test_codeLicenseDir_inp_valid_dummy_dir_settings_out_directory_exists(
    dummy_dir_settings,
):
    assert Path(dummy_dir_settings.codeLicensesDir).is_dir()


def test_dataLicenseDir_inp_valid_dummy_dir_settings_out_directory_exists(
    dummy_dir_settings,
):
    assert Path(dummy_dir_settings.dataLicensesDir).is_dir()


def test_cacheDir_inp_valid_dummy_dir_settings_out_directory_exists(dummy_dir_settings):
    assert Path(dummy_dir_settings.cacheDir).is_dir()


def test_courseConfigDir_inp_valid_dummy_dir_settings_out_directory_exists(
    dummy_dir_settings,
):
    assert Path(dummy_dir_settings.courseConfigDir).is_dir()


def test_coursExportDir_inp_valid_dummy_dir_settings_out_directory_exists(
    dummy_dir_settings,
):
    assert Path(dummy_dir_settings.courseConfigDir).is_dir()
