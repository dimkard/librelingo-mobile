# SPDX-FileCopyrightText: 2023 Dimitris Kardarakos <dimkard@posteo.net>
# SPDX-License-Identifier: GPL-3.0-or-later

from PySide2.QtCore import QObject, Slot
from glossaico.translator import _ as translate


class QQmlTranslator(QObject):
    """Exposes getttext translations to QML"""

    def __init__(self):
        QObject.__init__(self)

    @Slot(str, result=str)
    def _(self, message: str) -> str:
        """Expose the _ method to QML"""
        return translate(message)
