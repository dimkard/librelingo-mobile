# SPDX-FileCopyrightText: 2022 Dimitris Kardarakos <dimkard@posteo.net>
# SPDX-License-Identifier: GPL-3.0-or-later

from pathlib import Path
from datetime import datetime
import pytest
from sqlalchemy.orm import clear_mappers
from glossaico.progress_migration import ProgressMigration
from glossaico.practice_session_alchemy_store_keeper import (
    PracticeSessionAlchemyStoreKeeper,
)

# from glossaico.practice_session_data import PracticeSessionData, PassedChallenges
from glossaico.alchemy_database import AlchemyDatabase


@pytest.fixture(name="dummy_cfg_file_path")
def fixture_dummy_cfg_file_path(tmp_path_factory):
    cfg_dir = tmp_path_factory.mktemp("config")
    dummy_cfg = """active-course-name: spanish-from-english
active-user-id: default-user
config-version: v1
images-ready: true
silent-mode: false
users:
  default-user:
    courses:
      german-from-english:
        skills:
          11_plurals:
            stats:
              passed: []
              total_skill_challenges: 0
          1_food:
            stats:
              passed: []
              total_skill_challenges: 0
          2_animals:
            stats:
              passed:
              - b4b51ca6254c
              - 24ece2f8cf29
              total_skill_challenges: 48
          49bdd5c6-6ca3-11eb-9439-0242ac130002_hello:
            stats:
              passed:
              - d184cd3b1a6e
              - 2f80d56e417b
              - ddb55829ea96
              total_skill_challenges: 15
          7_clothes:
            stats:
              passed:
              - 6818b38347e5
              total_skill_challenges: 47
          8_nature:
            stats:
              passed: []
              total_skill_challenges: 0
      spanish-from-english:
        skills:
          10_verb-plurals:
            stats:
              passed: []
              total_skill_challenges: 0
          11_plurals:
            stats:
              passed: []
              total_skill_challenges: 0
          12_professions:
            stats:
              passed: []
              total_skill_challenges: 0
          13_preferences:
            stats:
              passed: []
              total_skill_challenges: 0
          14_phrases:
            stats:
              passed: []
              total_skill_challenges: 0
          15_adjectives:
            stats:
              passed: []
              total_skill_challenges: 0
          1_extra_food:
            stats:
              passed:
              - c1d86018d2cf
              - 0e05a473ef0b
              - cdb0f0556649
              total_skill_challenges: 38
          2_animals:
            stats:
              passed:
              - f3ec01d6882e
              - d8fba73a8b08
              - 390f4df6b328
              total_skill_challenges: 33
          7_clothes:
            stats:
              passed:
              - 7399df07ff7c
              - 88e378563ad7
              - 34a480762d4e
              total_skill_challenges: 47
          8_nature:
            stats:
              passed: []
              total_skill_challenges: 0
          9_verbs:
            stats:
              passed: []
              total_skill_challenges: 0
"""

    dummy_cfg_file_path = f"{cfg_dir}/general.yaml"

    with open(dummy_cfg_file_path, "w", encoding="utf-8") as f:
        f.write(dummy_cfg)

    return dummy_cfg_file_path


def test_migrate_inp_valid_config_file_out_practice_session_added_to_db(
    dummy_cfg_file_path,
):
    clear_mappers()
    db_loader = AlchemyDatabase("sqlite+pysqlite:///:memory:")
    engine = db_loader.load()
    keeper = PracticeSessionAlchemyStoreKeeper(engine)
    migrator = ProgressMigration(dummy_cfg_file_path, keeper)
    migrator.migrate()

    with keeper:
        session_data = keeper.store().getBySkillId("7", "spanish-from-english")[0]
        assert (
            (session_data.correct_challenges == 3)
            and (session_data.expected_challenges == 47)
            and (session_data.practice_dt <= datetime.now())
        )


def test_migrate_inp_valid_config_file_out_passed_challenges_added_to_db(
    dummy_cfg_file_path,
):
    clear_mappers()
    db_loader = AlchemyDatabase("sqlite+pysqlite:///:memory:")
    engine = db_loader.load()
    keeper = PracticeSessionAlchemyStoreKeeper(engine)
    migrator = ProgressMigration(dummy_cfg_file_path, keeper)
    migrator.migrate()

    with keeper:
        session_data = keeper.store().getBySkillId("2", "german-from-english")[0]
        passed_ids = [p.challenge_id for p in session_data.passed]
        assert (
            ("b4b51ca6254c" in passed_ids)
            and ("24ece2f8cf29" in passed_ids)
            and (len(passed_ids) == 2)
        )


def test_migrate_inp_valid_config_file_out_file_backed_up(dummy_cfg_file_path):
    clear_mappers()
    db_loader = AlchemyDatabase("sqlite+pysqlite:///:memory:")
    engine = db_loader.load()
    keeper = PracticeSessionAlchemyStoreKeeper(engine)
    migrator = ProgressMigration(dummy_cfg_file_path, keeper)
    migrator.migrate()

    assert (not Path(dummy_cfg_file_path).is_file()) and Path(
        f"{dummy_cfg_file_path}.bak"
    ).is_file()
