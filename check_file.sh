#!/bin/bash
if [[ $1 == *"tests/"* ]]; then
    RCFILE="tests/pylintrc"
else
    RCFILE="glossaico/pylintrc"
fi
echo RCFILE $RCFILE
echo -e "\n1. Format code:"
python3 -m black $1
echo -e "\n2. Check type hints:"
python3 -m mypy $1
echo -e "\n3. Check code quality:"
python3 -m pylint --rcfile=$RCFILE $1

