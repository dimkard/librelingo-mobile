# SPDX-FileCopyrightText: 2022 Dimitris Kardarakos <dimkard@posteo.net>
# SPDX-License-Identifier: GPL-3.0-or-later

import pytest
from sqlalchemy.orm import clear_mappers
from librelingo_types import Skill, Word, Phrase
from glossaico.alchemy_database import AlchemyDatabase
from glossaico.practice_session_alchemy_store_keeper import (
    PracticeSessionAlchemyStoreKeeper,
)
from glossaico.challenge_data_calculator import ChallengeDataCalculator
from glossaico.course_validation import CourseValidation
from glossaico.progress_calculator import ProgressCalculator
from glossaico.q_skills_model import QSkillsModel


@pytest.fixture(scope="module", name="dummy_challenge_data_calculator")
def fixture_dummy_challenge_data_calculator(tmp_path_factory):
    image_dir = tmp_path_factory.mktemp("image_dir")
    voice_dir = tmp_path_factory.mktemp("voice_dir")
    validator = CourseValidation(image_dir, voice_dir)
    export_dir = tmp_path_factory.mktemp("export_dir")
    challenge_data_calculator = ChallengeDataCalculator(export_dir, validator, 3, 4)

    def dummy_challenges():
        return []

    def dummy_levels():
        return 3

    challenge_data_calculator.validChallenges = dummy_challenges
    challenge_data_calculator.levels = dummy_levels

    return challenge_data_calculator


@pytest.fixture(scope="module", name="dummy_progress_calculator")
def fixture_dummy_progress_calculator(tmp_path_factory):
    db_dir = tmp_path_factory.mktemp("db_dir")
    clear_mappers()
    db_loader = AlchemyDatabase(f"sqlite+pysqlite:///{db_dir}/gloossaico.db")
    practice_keeper = PracticeSessionAlchemyStoreKeeper(db_loader.load())
    progress_calculator = ProgressCalculator(practice_keeper)

    return progress_calculator


@pytest.fixture(scope="module", name="test_model")
def fixture_test_model(dummy_progress_calculator, dummy_challenge_data_calculator):
    q_skills_model = QSkillsModel(
        dummy_progress_calculator, dummy_challenge_data_calculator
    )
    skill_1 = Skill(
        name="Animals",
        filename="basics/skills/animals.yaml",
        id=2,
        words=[
            Word(
                in_target_language=["perro", "can"],
                in_source_language=["dog", "hound"],
                pictures=["dog1", "dog2", "dog3"],
            ),
            Word(
                in_target_language=["gato"],
                in_source_language=["cat"],
                pictures=["cat1", "cat2", "cat3"],
            ),
            Word(
                in_target_language=["el perro", "el can"],
                in_source_language=["the dog", "the hound"],
                pictures=["dog1", "dog2", "dog3"],
            ),
        ],
        phrases=[
            Phrase(
                in_target_language=["Es un caballo"],
                in_source_language=["It's a horse"],
            ),
            Phrase(
                in_target_language=["Max es un perro", "Max es un can"],
                in_source_language=["Max is a dog"],
            ),
            Phrase(
                in_target_language=["Un pato y un caballo"],
                in_source_language=["A duck and a horse"],
            ),
        ],
        image_set=["dog1", "lion3", "cat3"],
        dictionary=[
            ("un", ("a/an (masc.)",), True),
            ("es", ("is",), True),
            ("Max", ("Max (name)",), True),
            ("y", ("and",), True),
        ],
        introduction="Hello animals",
    )

    skill_2 = Skill(
        name="Food",
        filename="basics/skills/food.yaml",
        id=1,
        words=[
            Word(
                in_target_language=["pan"],
                in_source_language=["bread"],
                pictures=["bread1", "bread2", "bread3"],
            ),
            Word(
                in_target_language=["leche"],
                in_source_language=["milk"],
                pictures=["milk1", "milk2", "milk3"],
            ),
            Word(
                in_target_language=["pasta"],
                in_source_language=["pasta"],
                pictures=["pasta1", "pasta2", "pasta3"],
            ),
        ],
        phrases=[
            Phrase(
                in_target_language=["Buen provecho"],
                in_source_language=["Enjoy your meal"],
            ),
            Phrase(in_target_language=["Por favor"], in_source_language=["Please"]),
            Phrase(
                in_target_language=["Pan, por favor"],
                in_source_language=["Bread, please"],
            ),
        ],
        image_set=["bread1", "milk2", "pasta3"],
        dictionary=[
            ("por", ("for",), True),
            ("favor", ("favor",), True),
            ("agua", ("water",), True),
        ],
        introduction=None,
    )
    q_skills_model.skills = [skill_1, skill_2]
    q_skills_model.courseName = "dummy-course"

    return q_skills_model


def test_skill_data_role_inp_valid_model_out_correct_name(test_model):
    skill_data = test_model.data(test_model.index(0, 0), QSkillsModel.DisplayObjectRole)

    assert skill_data["name"] == "Animals"


def test_skill_data_role_inp_valid_model_out_correct_introduction_exists(test_model):
    skill_data = test_model.data(test_model.index(0, 0), QSkillsModel.DisplayObjectRole)
    assert skill_data["introduction"]


def test_skill_data_role_inp_valid_model_out_correct_sample_words_exist(test_model):
    skill_data = test_model.data(test_model.index(0, 0), QSkillsModel.DisplayObjectRole)
    assert skill_data["sample_words"]


def test_skill_data_role_inp_valid_model_out_correct_sample_phrases_exist(test_model):
    skill_data = test_model.data(test_model.index(0, 0), QSkillsModel.DisplayObjectRole)
    assert skill_data["sample_phrases"]


def test_language_skill_role_inp_valid_model_out_correct_course_name(test_model):
    language_skill = test_model.data(
        test_model.index(0, 0), QSkillsModel.LanguageSkillRole
    )
    assert language_skill.courseName == "dummy-course"


def test_language_skill_role_inp_valid_model_out_zero_progress(test_model):
    language_skill = test_model.data(
        test_model.index(0, 0), QSkillsModel.LanguageSkillRole
    )
    assert language_skill.progress == 0
