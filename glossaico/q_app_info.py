# SPDX-FileCopyrightText: 2021 Dimitris Kardarakos <dimkard@posteo.net>
# SPDX-License-Identifier: GPL-3.0-or-later

from pathlib import Path
from PySide2.QtCore import QObject, Property
from glossaico import (
    APP_NAME,
    APP_COMPONENT_NAME,
    APP_AUTHOR,
    APP_AUTHOR_WEB_ADDR,
    APP_DESCRIPTION,
    APP_URL,
    APP_BUG_URL,
    APP_VERSION,
    APP_COPYRIGHT,
    APP_CREDITS,
)


class QAppInfo(QObject):
    """Class that returns application metadata"""

    def __init__(self, icon_dir: str, code_license_dir: str, data_license_dir: str):
        QObject.__init__(self)
        self._icon_dir = icon_dir
        self._code_license_dir = code_license_dir
        self._data_license_dir = data_license_dir
        self._data: dict = {}

    def load(self):
        """Populate the application data dictionary"""
        self._data = {
            "displayName": APP_NAME,
            "componentName": APP_COMPONENT_NAME,
            "shortDescription": APP_DESCRIPTION,
            "homepage": APP_URL,
            "bugAddress": APP_BUG_URL,
            "version": APP_VERSION,
            "programLogo": f"{self._icon_dir}/mascot-jetpack-noshadow.svg",
            "authors": [{"name": APP_AUTHOR, "webAddress": APP_AUTHOR_WEB_ADDR}],
            "credits": APP_CREDITS,
            "translators": [],
            "licenses": [],
            "copyrightStatement": APP_COPYRIGHT,
            "desktopFileName": "org.codeberg.dimkard.glossaico.desktop",
            "otherText": "",
            "dataLicenses": [],
        }

        self._addCodeLicenses()
        self._addDataLicenses()

    def _addCodeLicenses(self):
        license_dir = Path(self._code_license_dir)

        if not license_dir.is_dir():
            return

        for license_file_name in license_dir.iterdir():
            with open(license_file_name, "r", encoding="utf-8") as file:
                license_text = file.read()
                license_item = {
                    "name": license_file_name.stem,
                    "text": license_text,
                    "spdx": license_file_name.stem,
                }
                self._data["licenses"].append(license_item)

    def _addDataLicenses(self):
        license_dir = Path(self._data_license_dir)
        if not license_dir.is_dir():
            return

        for license_file_name in license_dir.iterdir():
            with open(license_file_name, "r", encoding="utf-8") as file:
                license_text = file.read()
                license_item = {
                    "name": license_file_name.stem,
                    "text": license_text,
                    "spdx": license_file_name.stem,
                }
                self._data["dataLicenses"].append(license_item)

    def readAboutData(self):
        """Returns the variant map that represents the about-data information
        of the application which is needed by the QML side
        """
        return self._data

    aboutData = Property("QVariantMap", readAboutData, constant=True)
