# SPDX-FileCopyrightText: 2023 Dimitris Kardarakos <dimkard@posteo.net>
# SPDX-License-Identifier: GPL-3.0-or-later

import json
from enum import Enum


class CoursesInfoLoader:
    """Class that provides information about the available courses"""

    class CourseStatus(Enum):
        """Represents the development status of a course"""

        STABLE = 1
        BETA = 2
        ALPHA = 3

    def __init__(self, courses_file_path: str):
        self._courses_json_path: str = courses_file_path

    def coursesInfo(self, minimum_status: CourseStatus):
        """Returns courses info with status at least @minimum_status"""
        courses = []

        if self._courses_json_path:
            courses = self._loadCoursesJsonFile()

        if minimum_status == CoursesInfoLoader.CourseStatus.STABLE:
            return [course for course in courses if course["inProduction"]]

        if minimum_status == CoursesInfoLoader.CourseStatus.BETA:
            return [course for course in courses if course["deploy"]]

        return courses

    def _loadCoursesJsonFile(self):
        with open(self._courses_json_path, "r", encoding="utf-8") as courses_json:
            all_courses = json.load(courses_json)

        return all_courses
