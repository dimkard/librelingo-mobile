# SPDX-FileCopyrightText: 2022 Dimitris Kardarakos <dimkard@posteo.net>
# SPDX-License-Identifier: GPL-3.0-or-later

import pytest
from glossaico.language_skill import LanguageSkill


@pytest.fixture(name="not_loaded_language_skill")
def fixture_not_loaded_language_skill():
    return LanguageSkill(None, None)


def test_skillId_inp_not_loaded_model_out_error(not_loaded_language_skill):
    with pytest.raises(RuntimeError):
        print(not_loaded_language_skill.skillId)


def test_displayName_inp_not_loaded_model_out_error(not_loaded_language_skill):
    with pytest.raises(RuntimeError):
        print(not_loaded_language_skill.displayName)


def test_words_inp_not_loaded_model_out_error(not_loaded_language_skill):
    with pytest.raises(RuntimeError):
        print(not_loaded_language_skill.words)


def test_phrases_inp_not_loaded_model_out_error(not_loaded_language_skill):
    with pytest.raises(RuntimeError):
        print(not_loaded_language_skill.phrases)


def test_name_inp_not_loaded_model_out_error(not_loaded_language_skill):
    with pytest.raises(RuntimeError):
        print(not_loaded_language_skill.name)


def test_courseName_inp_not_loaded_model_out_error(not_loaded_language_skill):
    with pytest.raises(RuntimeError):
        print(not_loaded_language_skill.courseName)


def test_progress_inp_not_loaded_model_out_error(not_loaded_language_skill):
    with pytest.raises(RuntimeError):
        print(not_loaded_language_skill.progress)


def test_challenges_inp_not_loaded_model_out_error(not_loaded_language_skill):
    with pytest.raises(RuntimeError):
        print(not_loaded_language_skill.challenges)


def test_needsReview_inp_not_loaded_model_out_error(not_loaded_language_skill):
    with pytest.raises(RuntimeError):
        print(not_loaded_language_skill.needsReview)


def test_challengesPassed_inp_not_loaded_model_out_error(not_loaded_language_skill):
    with pytest.raises(RuntimeError):
        print(not_loaded_language_skill.challengesPassed)
