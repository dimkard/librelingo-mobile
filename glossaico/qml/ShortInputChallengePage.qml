/*
 * SPDX-FileCopyrightText: 2023 Dimitris Kardarakos <dimkard@posteo.net>
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

import QtQuick 2.12
import QtQuick.Controls 2.12 as Controls
import QtQuick.Layouts 1.12
import org.kde.kirigami 2.12 as Kirigami
import org.codeberg.dimkard.glossaico 1.0 as Glossaico

AbstractChallengePage {
    id: root

    property string targetLanguage

    answerGiven: ""
    challengeItem: columnLayout
    title: QGlTr._("Translate")

    ColumnLayout {
        id: columnLayout

        anchors {
            top: parent.top
            right: parent.right
            left: parent.left
        }
        spacing: Kirigami.Units.largeSpacing

        Kirigami.Heading {
            text: QGlTr._("Translate <b>%1</b>").arg(root.challengeData.meaningInSourceLanguage)
            wrapMode: Text.WordWrap
            bottomPadding: Kirigami.Units.largeSpacing * 2
            leftPadding: Kirigami.Units.largeSpacing

            Layout.fillWidth: true
        }

        Controls.TextField {
            id: answerField

            placeholderText: QGlTr._("Type in %1").arg(root.targetLanguage)

            Layout.fillWidth: true

            onTextEdited: {
                root.answerGiven = {"answer" : text }
            }
        }

        Flow {
            spacing: Kirigami.Units.largeSpacing

            Repeater {
                id: specialCharsRepeater

                model: root.specialCharacters

                delegate: Controls.Button {
                    text: modelData
                    KeyNavigation.left: specialCharsRepeater.itemAt(model.index - 1)
                    KeyNavigation.right: specialCharsRepeater.itemAt(model.index + 1)


                    onClicked: {
                        answerField.insert(answerField.cursorPosition, modelData)
                        answerField.forceActiveFocus(Qt.MouseFocusReason)
                    }
                }
            }

            Layout.fillWidth: true
        }
    }

    Component.onCompleted: {
        if(!Kirigami.Settings.isMobile) {
            answerField.forceActiveFocus(Qt.ActiveWindowFocusReason)
        }
    }
}
