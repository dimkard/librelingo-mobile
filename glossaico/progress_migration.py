# SPDX-FileCopyrightText: 2023 Dimitris Kardarakos <dimkard@posteo.net>
# SPDX-License-Identifier: GPL-3.0-or-later

from pathlib import Path
from datetime import datetime
import logging
from yaml import safe_load, YAMLError
from glossaico.database_store_keeper import DatabaseStoreKeeper
from glossaico.practice_session_data import PracticeSessionData, PassedChallenges


class ProgressMigration:
    """Move Glossaico 1.0 progress data from yaml configuration file to database"""

    def __init__(
        self, config_file_path: str, practice_store_keeper: DatabaseStoreKeeper
    ):
        self.config_file_path = config_file_path
        self.store_keeper = practice_store_keeper

    def v1CofigurationFile(self) -> dict:
        """Read the 1.0 yaml configuration file"""
        if not Path(self.config_file_path).is_file():
            logging.debug("No configuration file found at %s", self.config_file_path)
            return {}

        cfg_data = {}

        with open(self.config_file_path, "r", encoding="utf-8") as cfg:
            try:
                cfg_data = safe_load(cfg)
            except YAMLError as exc:
                logging.error(exc)

        return cfg_data

    @staticmethod
    def v1Courses(cfg_data: dict) -> list:
        """Reads the yaml configuration courses"""

        if (
            ("users" in cfg_data)
            and ("default-user" in cfg_data["users"])
            and ("courses" in cfg_data["users"]["default-user"])
        ):
            return cfg_data["users"]["default-user"]["courses"]

        return []

    @staticmethod
    def v1CourseSkills(course_data: dict) -> list:
        """Returns the skills of a course that contain progress data"""

        if "skills" not in course_data:
            return []

        valid_skills = []
        for skill_name in course_data["skills"]:
            skill = course_data["skills"][skill_name]
            if (
                ("stats" in skill)
                and ("passed" in skill["stats"])
                and skill["stats"]["passed"]
                and ("total_skill_challenges" in skill["stats"])
                and (skill["stats"]["total_skill_challenges"] > 0)
            ):
                valid_skills.append((skill_name, skill))

        return valid_skills

    def migrate(self):
        """Migrate progress data"""
        cfg_data = self.v1CofigurationFile()

        if not cfg_data:
            logging.debug("No configuration data for migration found")
            return

        if ("config-version" not in cfg_data) or (cfg_data["config-version"] != "v1"):
            logging.info("Not a config for migration")
            return

        courses = self.v1Courses(cfg_data)

        if not courses:
            return

        for course_name in courses:
            course_skills = self.v1CourseSkills(courses[course_name])
            self.migrateSkills(course_name, course_skills)

        Path(self.config_file_path).replace(f"{self.config_file_path}.bak")

    def migrateSkills(self, course_name: str, course_skills: list):
        """Migrate the progress data of the skills of a course"""
        for skill_name, skill_data in course_skills:
            self.migrateSkill(course_name, skill_name, skill_data)

    def migrateSkill(self, course_name: str, skill_name: str, skill_data):
        """Migrate the progress data of a single skill"""

        skill_id_name = skill_name.split("_")

        passed = set()
        for passed_chal in skill_data["stats"]["passed"]:
            passed.add(PassedChallenges(passed_chal))

        total_amt = skill_data["stats"]["total_skill_challenges"]
        if len(skill_id_name) != 2:
            logging.error(
                "Skill %s of %s can't be migrated because the skill has not been found",
                skill_name,
                course_name,
            )
            return

        session_data = PracticeSessionData(
            course_name=course_name,
            practice_session_dt=datetime.now(),
            correct_challenges_amt=len(passed),
            expected_challenges_amt=total_amt,
            practiced_skill_id=skill_id_name[0],
            passed_challenges=passed,
        )

        with self.store_keeper:
            self.store_keeper.store().add(session_data)
            self.store_keeper.commit()
