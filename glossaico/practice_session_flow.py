# SPDX-FileCopyrightText: 2023 Dimitris Kardarakos <dimkard@posteo.net>
# SPDX-License-Identifier: GPL-3.0-or-later

from dataclasses import asdict, dataclass, field
from datetime import datetime
from random import sample
from typing import Optional, Set
import logging
from librelingo_utils import clean_word  # type: ignore
from glossaico.practice_session_data import PracticeSessionData, PassedChallenges
from glossaico.challenge_qa_creator import ChallengeQACreator
from glossaico.glossaico_types import ChallengeQuestion, ChallengeAnswer
from glossaico.language_skill import LanguageSkill


@dataclass
class ChallengeInfo:
    """Hosts challenge info"""

    data: dict = field(default_factory=dict)
    question: ChallengeQuestion = field(default_factory=ChallengeQuestion)
    answer: ChallengeAnswer = field(default_factory=ChallengeAnswer)


@dataclass
class SessionStatistics:
    """Hosts the amount of expected, incorrect and correct session challenges"""

    correct: int = 0
    expected: int = 0
    incorrect: int = 0


@dataclass
class SessionState:
    """Hosts the state of the session flow"""

    challenges: list = field(default_factory=list)
    challenges_to_repeat: list = field(default_factory=list)
    challenges_amt: int = 0
    stats: SessionStatistics = field(default_factory=SessionStatistics)
    passed_challenges: set = field(default_factory=set)
    current_challenge: ChallengeInfo = field(default_factory=ChallengeInfo)


class PracticeSessionFlow:
    """Class that controls the practice session flow
    - Within each challenge session asks again challenges answered incorrectly
    - Removes from the repeat queue a challenge if answered correctly
    at first asking within a challenge session
    """

    def __init__(self, validator, practice_keeper, progress_calculator) -> None:
        self._validator = validator
        self._language_skill: LanguageSkill = LanguageSkill(None, None)
        self._session_state = SessionState()
        self._repeat_mode = False
        self._practice_keeper = practice_keeper
        self._challenge_qa_creator: Optional[ChallengeQACreator] = None
        self._progress_calculator = progress_calculator

    @property
    def currentChallengeQuestion(self) -> dict:
        """Returns the question data of the current challenge. The returned type
        should be a ChallengeQuestion dataclass derivative converted to dictionary
        """

        return asdict(self._session_state.current_challenge.question)

    @property
    def practiceProgress(self) -> float:
        """Returns the percentage of the practice session that has been completed"""
        return (
            self._session_state.challenges_amt - len(self._session_state.challenges) - 1
        ) / self._session_state.challenges_amt

    @property
    def challengesAnswered(self) -> int:
        """Returns the amount of challenges already answered in the current
        practice session
        """

        return self._session_state.stats.correct + self._session_state.stats.incorrect

    @staticmethod
    def excludePassed(challenges: list, passed: Set[str]) -> list:
        """Remove the passed challenges from a list of challenges"""
        result = []
        for vc in challenges:
            if vc["id"] not in passed:
                result.append(vc)

        return result

    @staticmethod
    def expectedChallengesAmt(language_skill: LanguageSkill) -> int:
        """Returns the amount of challenges that should be practiced"""
        challenges_per_level = len(language_skill.challenges) / language_skill.levels

        return int(challenges_per_level * 1.2)

    @staticmethod
    def expectedGroupsAmt(language_skill: LanguageSkill, challenges_amt: int) -> int:
        """Returns the amount of groups that should be practiced"""

        skill_groups = {*(x["group"] for x in language_skill.challenges)}
        logging.debug("skill groups %s", skill_groups)
        if not skill_groups:
            return 0

        challenges_per_group = len(language_skill.challenges) / len(skill_groups)
        return round(challenges_amt / challenges_per_group)

    @staticmethod
    def groupChallengesSample(groups: set, challenges: list, groups_amt: int):
        """Returns a set of challenges that belongs in a sample of the
        groups provided"""

        fresh_groups_to_practice = sample([*groups], min(len(groups), groups_amt))
        return [x for x in challenges if x["group"] in fresh_groups_to_practice]

    @staticmethod
    def challengesSample(
        challenge_subset: list, expected_groups_amt: int, all_challenges: list
    ):
        """Generate a set of challenges from @challenge_subset that belong
        in @expected_challenges_amt. If the @expected_challenges_amt target
        has not been reached, add challenges from @all_challenges"""

        preferred_challenges = []
        remaining_challenges = []

        preferred_groups = {*([x["group"] for x in challenge_subset])}
        if len(preferred_groups) > 0:
            preferred_challenges = PracticeSessionFlow.groupChallengesSample(
                preferred_groups, all_challenges, expected_groups_amt
            )

        all_groups = {*(x["group"] for x in all_challenges)}
        if len(preferred_groups) < expected_groups_amt:
            # there are still groups to offer,
            # pick challenges from the already seen groups
            stale_groups = {*([x for x in all_groups if x not in preferred_groups])}
            remaining_challenges = PracticeSessionFlow.groupChallengesSample(
                stale_groups,
                all_challenges,
                expected_groups_amt - len(preferred_groups),
            )

        return remaining_challenges + preferred_challenges

    @staticmethod
    def filterBySilentMode(challenges: list, silent_mode: bool):
        """If silent_mode is True, excludes the listening challenges"""
        if not silent_mode:
            return challenges

        return [x for x in challenges if x["type"] != "listeningExercise"]

    def loadSessionChallenges(
        self, language_skill: LanguageSkill, silent_mode: bool
    ) -> bool:
        """Loads the challenges to be practiced in the session.
        Returns True if there is a valid set of challenges to practice"""

        if not language_skill:
            raise RuntimeError("No skill data found, aborting practice")

        self._language_skill = language_skill

        model_challenges = PracticeSessionFlow.filterBySilentMode(
            language_skill.challenges, silent_mode
        )

        fresh_challenges = PracticeSessionFlow.excludePassed(
            model_challenges, language_skill.challengesPassed
        )

        expected_challenges_amt = PracticeSessionFlow.expectedChallengesAmt(
            language_skill
        )

        expected_groups_amt = PracticeSessionFlow.expectedGroupsAmt(
            language_skill, expected_challenges_amt
        )

        if expected_groups_amt == 0:
            return False

        if len(fresh_challenges) == 0:
            session_challenges = sample(
                model_challenges,
                round(expected_challenges_amt),
            )
        else:
            session_challenges = self.challengesSample(
                fresh_challenges, expected_groups_amt, model_challenges
            )

        logging.debug(
            "session groups %s", {*([x["group"] for x in session_challenges])}
        )

        session_challenges = sorted(
            session_challenges,
            key=lambda x: (x["priority"]),
            reverse=True,
        )

        self._session_state = SessionState(
            challenges=session_challenges,
            challenges_to_repeat=[],
            challenges_amt=len(session_challenges),
            stats=SessionStatistics(
                correct=0, expected=expected_challenges_amt, incorrect=0
            ),
            passed_challenges=set(),
            current_challenge=ChallengeInfo(),
        )

        self._challenge_qa_creator = ChallengeQACreator(
            self._language_skill.phrases, self._language_skill.words, self._validator
        )

        return self._session_state.challenges_amt > 0

    def loadNextChallenge(self) -> bool:
        """If there are remaining challenges in the practice session, it
        initializes the challenge to be offered to the user, e.g. currentChallengeQuestion
        should be set accordingly. Returns True if such a challenge exists.
        """

        if not self._challenge_qa_creator:
            raise RuntimeError("No challenge creator is available")

        if (not self._session_state.challenges) and (
            not self._session_state.challenges_to_repeat
        ):
            return False

        if self._session_state.challenges:
            current_challenge_data = self._session_state.challenges.pop()
            self._repeat_mode = False
        elif self._session_state.challenges_to_repeat:
            current_challenge_data = self._session_state.challenges_to_repeat.pop()
            self._repeat_mode = True

        cc_question, cc_answer = self._challenge_qa_creator.qaTuple(
            current_challenge_data
        )
        self._session_state.current_challenge = ChallengeInfo(
            data=current_challenge_data, question=cc_question, answer=cc_answer
        )
        return True

    def _checkAnswer(self, answer_given: dict) -> bool:
        if self._session_state.current_challenge.question.challengeType == "chips":
            is_correct = (
                answer_given["answer"]
                in self._session_state.current_challenge.answer.formInTargetLanguage
            )
        else:
            is_correct = clean_word(answer_given["answer"].lower()) in [
                clean_word(f.lower())
                for f in self._session_state.current_challenge.answer.formInTargetLanguage
            ]

        return is_correct

    def checkResult(self, answer_given: dict) -> bool:
        """Returns True if the answer given is the correct one for the current
        challenge
        """

        is_correct = self._checkAnswer(answer_given)

        if not is_correct:
            self._session_state.challenges_to_repeat.append(
                self._session_state.current_challenge.data
            )

        if not self._repeat_mode:
            self._updateSessionStats(is_correct)

        return is_correct

    def _updateSessionStats(self, result: bool):
        if result:
            self._session_state.stats.correct = self._session_state.stats.correct + 1

        result_challenge_id = self._session_state.current_challenge.data["id"]
        session_passed_challenge_ids = [
            x.challenge_id for x in self._session_state.passed_challenges
        ]

        if (
            result
            and (result_challenge_id not in self._language_skill.challengesPassed)
            and (result_challenge_id not in session_passed_challenge_ids)
        ):
            self._session_state.passed_challenges.add(
                PassedChallenges(result_challenge_id)
            )

        if not result:
            self._session_state.stats.incorrect = (
                self._session_state.stats.incorrect + 1
            )

    def updatePracticeStorage(self):
        """Updates the persistent storage that hosts the practice session data"""
        practice_data = PracticeSessionData(
            self._language_skill.courseName,
            datetime.now(),
            self._session_state.stats.correct,
            self._session_state.stats.expected,
            self._language_skill.skillId,
            self._session_state.passed_challenges,
        )

        with self._practice_keeper:
            self._practice_keeper.store().add(practice_data)
            self._practice_keeper.commit()
