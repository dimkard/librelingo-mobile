# SPDX-FileCopyrightText: 2023 Dimitris Kardarakos <dimkard@posteo.net>
# SPDX-License-Identifier: GPL-3.0-or-later

import pytest
from glossaico.q_app_info import QAppInfo

DATA_LICENSE_NAME = "CC-BY-NC-4.0"
CODE_LICENSE_NAME = "GPL-3.0-or-later"


@pytest.fixture(scope="function", name="code_license_dir")
def fixture_code_license_dir(tmp_path_factory):
    """Provide a code license directory"""

    lic_dir = tmp_path_factory.mktemp("code_lic_dir")

    with open(f"{lic_dir}/{CODE_LICENSE_NAME}.txt", "w", encoding="utf-8") as code_lic:
        code_lic.write("code license")

    return lic_dir


@pytest.fixture(scope="function", name="data_license_dir")
def fixture_data_license_dir(tmp_path_factory):
    """Provide a data license directory"""

    lic_dir = tmp_path_factory.mktemp("data_lic_dir")

    with open(f"{lic_dir}/{DATA_LICENSE_NAME}.txt", "w", encoding="utf-8") as data_lic:
        data_lic.write("data license")

    return lic_dir


def test_load_inp_valid_code_licence_dir_out_code_license_name_exists(code_license_dir):
    test_info = QAppInfo("", code_license_dir, "invalid_dir")
    test_info.load()
    print(test_info.aboutData)
    assert test_info.aboutData["licenses"][0]["name"] == CODE_LICENSE_NAME


def test_load_inp_valid_code_licence_dir_out_code_license_text_exists(code_license_dir):
    test_info = QAppInfo("", code_license_dir, "test")
    test_info.load()
    assert test_info.aboutData["licenses"][0]["text"]


def test_load_inp_valid_data_license_out_data_license_name_exists(data_license_dir):
    test_info = QAppInfo("", "invalid_dir", data_license_dir)
    test_info.load()
    assert test_info.aboutData["dataLicenses"][0]["name"] == DATA_LICENSE_NAME


def test_load_inp_valid_data_license_out_data_license_text_exists(data_license_dir):
    test_info = QAppInfo("", "invalid_dir", data_license_dir)
    test_info.load()
    assert test_info.aboutData["dataLicenses"][0]["text"]
