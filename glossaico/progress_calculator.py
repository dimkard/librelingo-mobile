# SPDX-FileCopyrightText: 2023 Dimitris Kardarakos <dimkard@posteo.net>
# SPDX-License-Identifier: GPL-3.0-or-later

from typing import Set
from datetime import datetime, timedelta
from glossaico.glossaico_types import SkillData


class ProgressCalculator:
    """Calculates the progress of a skill"""

    def __init__(self, practice_keeper) -> None:
        self._practice_keeper = practice_keeper
        self.loaded: bool = False
        self._skill_data: SkillData = SkillData()
        self._practice_sessions: list = []
        self._levels: int = 0

    def load(self, skill_data: SkillData, levels: int):
        """Loads a specific skill for progress calculation"""
        if (not skill_data.skill_id) or (levels <= 0) or (not skill_data.course_name):
            raise RuntimeError(
                "Missing skill information, progress can't be calculated"
            )

        self._skill_data = skill_data
        self._levels = levels
        self.loaded = True

    def validateLoad(self):
        """Raise an error if the object has not been loaded"""
        if not self.loaded:
            raise RuntimeError("No skill has been loaded for calculation")

    def skillProgress(self) -> float:
        """Calculates the skill progress"""
        self.validateLoad()
        with self._practice_keeper:
            prc_sessions = self._practice_keeper.store().getBySkillId(
                self._skill_data.skill_id, self._skill_data.course_name
            )

            progress: float = 0

            for prc_session in prc_sessions:
                progress += (
                    prc_session.correct_challenges / prc_session.expected_challenges
                ) / self._levels

            return progress

    def needsReview(self) -> bool:
        """Returns true if a completed skill should be reviewed"""

        def fibo(num: int):
            return fibo(num - 1) + fibo(num - 2) if num > 1 else 1

        if not self.loaded:
            raise RuntimeError("No skill has been loaded for calculation")

        progress = self.skillProgress()

        with self._practice_keeper:
            prc_sessions = self._practice_keeper.store().getBySkillId(
                self._skill_data.skill_id, self._skill_data.course_name
            )

            prc_session_amt = len(prc_sessions)

            if prc_session_amt == 0:
                return False

            last_practice_dt = self.lastPracticeDate()

            if progress >= 1:
                days_till_next_practice = fibo(prc_session_amt)
                should_be_stale_at = last_practice_dt + timedelta(
                    days=days_till_next_practice
                )
                return should_be_stale_at < datetime.now()

            return False

    def lastPracticeDate(self):
        """Returns the date of the most recent practice"""
        self.validateLoad()

        with self._practice_keeper:
            prc_sessions = self._practice_keeper.store().getBySkillId(
                self._skill_data.skill_id, self._skill_data.course_name
            )

            last_practice_dt = prc_sessions[0].practice_dt

            for prc_session in prc_sessions:
                if prc_session.practice_dt > last_practice_dt:
                    last_practice_dt = prc_session.practice_dt

            return last_practice_dt

    def challengesPassed(self) -> Set[str]:
        """Returns the set of ids of the challenges marked as passed after answered correctly"""
        self.validateLoad()

        with self._practice_keeper:
            prc_sessions = self._practice_keeper.store().getBySkillId(
                self._skill_data.skill_id, self._skill_data.course_name
            )

            passed = set()

            for prc_session in prc_sessions:
                passed.update(prc_session.passed)

            return {p.challenge_id for p in passed}
