/*
 * SPDX-FileCopyrightText: 2023 Dimitris Kardarakos <dimkard@posteo.net>
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

import QtQuick 2.12
import QtQuick.Controls 2.12 as Controls
import org.kde.kirigami 2.12 as Kirigami
import QtQuick.Layouts 1.12

Kirigami.ScrollablePage {
    id: root

    property bool showGlobalHandle: true

    title: QGlTr._("Settings")

    ColumnLayout {
        Kirigami.FormLayout {
            Controls.CheckBox {
                id: silentMode

                checked: QConfiguration.silentMode

                Kirigami.FormData.label: QGlTr._("Silent mode")

                onToggled: QConfiguration.silentMode = checked
            }

            Controls.CheckBox {
                id: showBetaCourses

                checked: QConfiguration.showBetaCourses

                Kirigami.FormData.label: QGlTr._("Show courses in beta phase")

                onToggled: QConfiguration.showBetaCourses = checked
            }
            Layout.fillWidth: true
        }

        Kirigami.InlineMessage {
            visible: silentMode.checked
            type: Kirigami.MessageType.Information
            text: QGlTr._("Sounds and listening challenges have been disabled")

            Layout.fillWidth: true
        }

        Kirigami.InlineMessage {
            visible: showBetaCourses.checked
            type: Kirigami.MessageType.Information
            text: QGlTr._("Courses that may contain minor bugs will be shown")

            Layout.fillWidth: true
        }
    }
}
