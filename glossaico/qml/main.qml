/*
 * SPDX-FileCopyrightText: 2021 Dimitris Kardarakos <dimkard@posteo.net>
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

import QtQuick 2.12
import QtQuick.Controls 2.12 as Controls
import org.kde.kirigami 2.12 as Kirigami
import QtQuick.Layouts 1.12
import org.codeberg.dimkard.glossaico 1.0 as Glossaico

Kirigami.ApplicationWindow
{
    id: root

    property var latestDataInfo
    property Glossaico.QCourseData activeCourse

    width: Kirigami.Units.gridUnit * 30

    pageStack.globalToolBar.style: Kirigami.ApplicationHeaderStyle.ToolBar

    globalDrawer: LLMGlobalDrawer {
        courses: coursesView
        pageStack: root.pageStack
        handleVisible: pageStack.currentItem && pageStack.currentItem.hasOwnProperty("showGlobalHandle") && pageStack.currentItem.showGlobalHandle
    }

    contextDrawer: Kirigami.ContextDrawer {}

    pageStack {
        defaultColumnWidth: Kirigami.Units.gridUnit * 40
        initialPage: QCourseUpdateManager.canUpdate() ? dataLoadPage : coursesView
    }

    function startPractice() {
        pageStack.clear()
        if(QPracticeSessionFlow.loadNextChallenge ()) {
            pageStack.push(challengePageContainer[QPracticeSessionFlow.currentChallengeQuestion.challengeType], {
                challengeData: QPracticeSessionFlow.currentChallengeQuestion,
                progress: QPracticeSessionFlow.practiceProgress()
            })
        }
    }

    function endPractice() {
        pageStack.clear()
        pageStack.push([modulesView, skillsView])
        QPracticeSessionFlow.endPractice()
    }

    Component {
        id: dataLoadPage

        DataLoadPage {
            onDataLoadCompleted: {
                pageStack.clear()
                pageStack.push(coursesView)
            }
        }
    }

    Component {
        id: coursesView

        CoursesView {
            onCourseLoaded: {
                pageStack.clear()
                root.activeCourse = courseData
                pageStack.push(modulesView)
            }
        }
    }

    Component {
        id: modulesView

        ModulesView {
            course: root.activeCourse

            onModuleSelected: pageStack.push(skillsView)
        }
    }

    Component {
        id: skillsView

        SkillsView {

           onSkillSelected: {
                if(QPracticeSessionFlow.loadSessionChallenges(languageSkill, QConfiguration.silentMode)) {
                    pageStack.clear()
                    pageStack.push(skillPage, {
                        skillName: skillData.name,
                        skillWordsAndPhrases: skillData.sample_words || skillData.sample_phrases,
                        skillImage: skillData.sample_image,
                        introduction: skillData.introduction
                    })
                }
                else {
                    showPassiveNotification(QGlTr._("You can't practice %1, invalid or incomplete skill data.".arg(skillData.name)))
                }
            }
        }
    }

    Component {
        id: skillPage

        SkillPage {
            onSessionStart: root.startPractice()
            onSessionEnd: root.endPractice()
        }
    }

    Connections {
        target: QPracticeSessionFlow

        function onContinueSession() {
            pageStack.clear()
            pageStack.push(
                challengePageContainer[QPracticeSessionFlow.currentChallengeQuestion.challengeType],
                {
                    targetLanguage: root.activeCourse.targetLanguageName,
                    specialCharacters: root.activeCourse.specialCharacters,
                    courseName: root.activeCourse.name,
                    challengeData: QPracticeSessionFlow.currentChallengeQuestion,
                    progress: QPracticeSessionFlow.practiceProgress()
                }
            )
        }

        function onSessionCanceled() {
            root.endPractice()
        }

        function onSessionCompleted() {
            pageStack.clear()
            pageStack.push(challengeCompletePage)
        }
    }

    Component {
        id: challengeCompletePage

        ChallengeCompletePage {
            challengesAnswered: QPracticeSessionFlow.challengesAnswered

            onSessionEnd: root.endPractice()
        }
    }

    ChallengePagesContainer {
        id: challengePageContainer
    }

    Item {
        states: [
            State {
                name: ""
                PropertyChanges {target: pageStack; interactive: true}
            },
            State {
                name: "practicing"
                when: QPracticeSessionFlow.practicing
                PropertyChanges {target: pageStack; interactive: false}
                PropertyChanges {target: pageStack; defaultColumnWidth: root.width}
            }
        ]
    }
}
