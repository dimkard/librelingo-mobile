# SPDX-FileCopyrightText: 2023 Dimitris Kardarakos <dimkard@posteo.net>
# SPDX-License-Identifier: GPL-3.0-or-later

from abc import ABC, abstractmethod


class PropertyStore(ABC):
    """The interface of the property store classes"""

    @abstractmethod
    def get(self):
        """Returns the property store from the persistent storage"""

    @abstractmethod
    def upsert(self, model):
        """Updates the store by replacing the saved model with the model provided"""
