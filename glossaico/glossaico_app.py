#!/usr/bin/env python3

# SPDX-FileCopyrightText: 2023 Dimitris Kardarakos <dimkard@posteo.net>
# SPDX-License-Identifier: GPL-3.0-or-later

from pathlib import Path
import logging
import argparse
from PySide2.QtGui import QIcon
from PySide2.QtCore import QUrl, QStandardPaths
from PySide2.QtWidgets import QApplication
from PySide2.QtQml import QQmlApplicationEngine, qmlRegisterType
from glossaico.q_modules_model import QModulesModel
from glossaico.q_skills_model import QSkillsModel
from glossaico.q_language_course import QCourseLoader, QCourseData
from glossaico.q_practice_session_flow import QPracticeSessionFlow
from glossaico.q_course_update_manager import QCourseUpdateManager
from glossaico.practice_session_flow import PracticeSessionFlow
from glossaico.q_app_info import QAppInfo
from glossaico.q_courses_model import QCoursesModel
from glossaico.cfg_parser_property_store import CfgParserPropertyStore
from glossaico.q_configuration import QConfiguration
from glossaico.fallback_theme import FallbackTheme
from glossaico.directory_settings import DirectorySettings
from glossaico.course_validation import CourseValidation
from glossaico.q_dir_settings import QDirSettings
from glossaico.alchemy_database import AlchemyDatabase
from glossaico.practice_session_alchemy_store_keeper import (
    PracticeSessionAlchemyStoreKeeper,
)
from glossaico.progress_calculator import ProgressCalculator
from glossaico.challenge_data_calculator import ChallengeDataCalculator
from glossaico.progress_migration import ProgressMigration
from glossaico.database_migration import DatabaseMigration
from glossaico import APP_COMPONENT_NAME
from glossaico.q_qml_translator import QQmlTranslator
from glossaico.translator import _

# pylint: disable=unused-import
import glossaico.rc_glossaico_breeze


def main():
    """Initializes and manages the application execution"""
    # pylint: disable=too-many-locals
    app = QApplication()
    engine = QQmlApplicationEngine()
    parser = argparse.ArgumentParser(
        prog="glossaico",
        description=_(
            "Glossaico is a language learning application based on LibreLingo"
        ),
    )
    parser.add_argument(
        "-l",
        "--log",
        help=_("set the logging level"),
        choices=["DEBUG", "INFO", "WARNING", "ERROR", "CRITICAL"],
        default="WARNING",
    )
    args = parser.parse_args()

    root_logger = logging.getLogger()
    root_logger.setLevel(args.log)

    singletons = {
        "QConfiguration": None,
        "QAppInfo": None,
        "QCourseUpdateManager": None,
        "QCourseLoader": None,
        "QPracticeSessionFlow": None,
        "QDirSettings": None,
        "QSkillsModel": None,
        "QCoursesModel": None,
        "QGlTr": None,
    }

    qmlRegisterType(
        QModulesModel, "org.codeberg.dimkard.glossaico", 1, 0, "QModulesModel"
    )
    qmlRegisterType(QCourseData, "org.codeberg.dimkard.glossaico", 1, 0, "QCourseData")

    context = engine.rootContext()

    config_store = CfgParserPropertyStore(
        f"{QStandardPaths.writableLocation(QStandardPaths.GenericConfigLocation)}"
        f"/{APP_COMPONENT_NAME}"
    )
    config_store.load("app-config")

    singletons["QConfiguration"] = QConfiguration(config_store)

    QIcon.setFallbackThemeName(FallbackTheme(QIcon.themeSearchPaths()).name)

    dir_settings = DirectorySettings(
        QStandardPaths.writableLocation(QStandardPaths.GenericDataLocation),
        QStandardPaths.writableLocation(QStandardPaths.CacheLocation),
        APP_COMPONENT_NAME,
    )

    singletons["QDirSettings"] = QDirSettings(dir_settings)
    singletons["QAppInfo"] = QAppInfo(
        dir_settings.iconDir, dir_settings.codeLicensesDir, dir_settings.dataLicensesDir
    )
    singletons["QAppInfo"].load()
    singletons["QCourseUpdateManager"] = QCourseUpdateManager(
        dir_settings.appDataDir, dir_settings.cacheDir, singletons["QConfiguration"]
    )

    singletons["QCoursesModel"] = QCoursesModel(
        f"{dir_settings.appDataDir}/courses.json"
    )

    if singletons["QConfiguration"].installedDataCheckSum and (
        config_store.get().configVersion == "v3"
    ):
        singletons["QCoursesModel"].loadData(
            singletons["QConfiguration"].showBetaCourses
        )

    singletons["QConfiguration"].coursesUpdated.connect(
        singletons["QCoursesModel"].loadData
    )

    singletons["QCourseLoader"] = QCourseLoader(
        dir_settings.cacheDir,
        dir_settings.courseConfigDir,
        singletons["QConfiguration"].installedDataCheckSum,
    )
    validator = CourseValidation(dir_settings.imageDir, dir_settings.voiceDir)

    db_file_str_path = f"{dir_settings.appDataDir}/glossaico.db"
    if not Path(db_file_str_path).is_file():
        with open(db_file_str_path, "w", encoding="utf-8"):
            pass

    db_loader = AlchemyDatabase(f"sqlite+pysqlite:///{db_file_str_path}")
    db_engine = db_loader.load()

    db_migration_cm = DatabaseMigration(db_engine)
    with db_migration_cm:
        db_migration_cm.migrate()
    practice_keeper = PracticeSessionAlchemyStoreKeeper(db_engine)

    progress_calculator = ProgressCalculator(practice_keeper)
    practice_session_flow = PracticeSessionFlow(
        validator, practice_keeper, progress_calculator
    )
    singletons["QPracticeSessionFlow"] = QPracticeSessionFlow(practice_session_flow)
    challenge_data_calculator = ChallengeDataCalculator(
        dir_settings.courseExportDir,
        validator,
        singletons["QConfiguration"].optionsDisplayAmt,
        singletons["QConfiguration"].cardsDisplayAmt,
    )

    singletons["QSkillsModel"] = QSkillsModel(
        progress_calculator, challenge_data_calculator
    )
    # pylint: disable=no-member
    singletons["QPracticeSessionFlow"].sessionCompleted.connect(
        singletons["QSkillsModel"].updateModel
    )

    singletons["QGlTr"] = QQmlTranslator()

    # Set context properties (use singletons on Qt6)
    for name, instance in singletons.items():
        context.setContextProperty(name, instance)

    cfg_loc = QStandardPaths.writableLocation(QStandardPaths.GenericConfigLocation)
    progress_mig = ProgressMigration(
        f"{cfg_loc}/{APP_COMPONENT_NAME}/general.yaml",
        practice_keeper,
    )
    progress_mig.migrate()

    url = QUrl(f"file://{Path(Path(__file__).parent).resolve()}/qml/main.qml")
    engine.load(url)

    if len(engine.rootObjects()) == 0:
        # pylint: disable=consider-using-sys-exit
        quit()

    app.exec_()


if __name__ == "__main__":
    main()
