# SPDX-FileCopyrightText: 2022 Dimitris Kardarakos <dimkard@posteo.net>
# SPDX-License-Identifier: GPL-3.0-or-later


from unittest import mock
import pytest
from librelingo_types import Course, Language, License, Settings
from glossaico.language_course import CourseCache, CourseLoader


@pytest.fixture(scope="function", name="dummy_course")
def fixture_dummy_course():
    """Return a sample course for testing"""
    course = Course(
        target_language=Language("English", "en"),
        source_language=Language("Spanish", "es"),
        special_characters=[],
        modules=[],
        license=License(
            full_name="Attribution 4.0 International (CC BY 4.0)",
            name="CC BY 4.0",
            link="https://creativecommons.org/licenses/by/4.0/",
        ),
        dictionary=[],
        repository_url="https://example.com",
        course_dir="some_language/course",
        settings=Settings(),
    )

    return course


@pytest.fixture(scope="function", name="dummy_course_cache")
def fixture_dummy_course_cache(tmp_path_factory):
    cache_dir = tmp_path_factory.mktemp("cache")
    course_name = "test-spanish-from-english"
    course_cache = CourseCache(cache_dir, course_name, "test-check_sum-1")

    return course_cache


class TestCourseCache:
    def test_save_inp_valid_course_out_binary_file_created(
        self, dummy_course, dummy_course_cache
    ):
        dummy_course_cache.save(dummy_course)

        assert dummy_course_cache._pickledCoursePath(
            dummy_course_cache._course_name
        ).is_file()

    def test_load_inp_valid_binary_course_out_lili_course(
        self, dummy_course, dummy_course_cache
    ):
        dummy_course_cache.save(dummy_course)
        loaded_course = dummy_course_cache.load()
        assert loaded_course.repository_url == dummy_course.repository_url

    def test_exists_inp_valid_binary_course_returns_true(
        self, dummy_course_cache, dummy_course
    ):
        dummy_course_cache.save(dummy_course)
        assert dummy_course_cache.exists()

    def test_exists_inp_no_cached_course_returns_false(self, dummy_course_cache):
        assert not dummy_course_cache.exists()


class TestCourseLoader:
    def test_load_inp_valid_pickled_course_out_lili_course(
        self, dummy_course, dummy_course_cache, tmp_path_factory
    ):
        dummy_course_cache.save(dummy_course)
        courseLoader = CourseLoader(
            dummy_course_cache._cache_dir,
            tmp_path_factory,
            dummy_course_cache._installed_checksum,
        )
        loaded_course = courseLoader.load(dummy_course_cache._course_name)
        assert loaded_course == dummy_course

    @mock.patch("glossaico.language_course.load_course")
    def test_load_inp_new_course_out_lili_course(
        self, mc_load_course, dummy_course, dummy_course_cache, tmp_path_factory
    ):
        courseLoader = CourseLoader(
            dummy_course_cache._cache_dir,
            tmp_path_factory,
            dummy_course_cache._installed_checksum,
        )
        mc_load_course.return_value = dummy_course
        loaded_course = courseLoader.load(dummy_course_cache._course_name)
        assert loaded_course == dummy_course
