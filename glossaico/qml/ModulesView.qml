/*
 * SPDX-FileCopyrightText: 2021 Dimitris Kardarakos <dimkard@posteo.net>
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

import QtQuick 2.12
import QtQuick.Controls 2.12 as Controls
import org.kde.kirigami 2.12 as Kirigami
import org.codeberg.dimkard.glossaico 1.0 as Glossaico

Kirigami.ScrollablePage {
    id: root

    property var course
    property bool showGlobalHandle: true

    title: QGlTr._("Modules")

    signal moduleSelected(string moduleName)

    Kirigami.PlaceholderMessage {
        anchors.centerIn: parent

        visible: view.count === 0
        width: parent.width - (Kirigami.Units.largeSpacing * 4)
        text: QGlTr._("No module found")
    }

    ListView {
        id: view

        enabled: modulesModel && count > 0
        model: modulesModel
        spacing: Kirigami.Units.largeSpacing

        delegate: Kirigami.BasicListItem {
            id: itemDelegate

            reserveSpaceForIcon: false
            label: model && model.title
            topPadding: Kirigami.Units.gridUnit
            bottomPadding: Kirigami.Units.gridUnit

            function trigger() {
                QSkillsModel.courseName = root.course.name
                QSkillsModel.skills = model.skills
                root.moduleSelected(model.title)
            }

            onClicked: trigger()
        }
    }

    Glossaico.QModulesModel {
        id: modulesModel

        courseModules: root.course.courseModules
    }
}
