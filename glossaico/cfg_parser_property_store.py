# SPDX-FileCopyrightText: 2023 Dimitris Kardarakos <dimkard@posteo.net>
# SPDX-License-Identifier: GPL-3.0-or-later

from pathlib import Path
import configparser
from glossaico.property_store import PropertyStore
from glossaico.app_configuration import ApplicationConfiguration


class CfgParserPropertyStore(PropertyStore):
    """Property store that makes use of a Python configuration parser"""

    def __init__(self, store_directory: str):
        self._store_directory_path: Path = Path(store_directory)
        self._store_path = Path("/tmp")
        self._store = configparser.ConfigParser()
        self.loaded = False

    def load(self, store_name: str):
        """Loads the property store from the persistent storage"""
        self._store_path = self._createStoreFile(store_name)
        self._store.read(self._store_path.resolve())
        self._createGroups()
        self.loaded = True

    def get(self) -> ApplicationConfiguration:
        if not self.loaded:
            raise RuntimeError("The property store has not been loaded")

        default_config = ApplicationConfiguration()

        cfg_version = (
            self._store["general"]["config-version"]
            if "config-version" in self._store["general"]
            else default_config.configVersion
        )

        inst_check_sum = (
            self._store["general"]["installed-data-check-sum"]
            if "installed-data-check-sum" in self._store["general"]
            else default_config.installedDataCheckSum
        )

        silent_mode: bool = (
            self._store["user-settings"].getboolean("silent-mode")
            if "silent-mode" in self._store["user-settings"]
            else default_config.silentMode
        )

        show_beta_courses: bool = (
            self._store["user-settings"].getboolean("show-beta-courses")
            if "show-beta-courses" in self._store["user-settings"]
            else default_config.showBetaCourses
        )

        return ApplicationConfiguration(
            inst_check_sum, silent_mode, cfg_version, show_beta_courses
        )

    def upsert(self, model: ApplicationConfiguration):

        self._store["general"]["config-version"] = model.configVersion
        self._store["general"]["installed-data-check-sum"] = model.installedDataCheckSum
        self._store["user-settings"]["silent-mode"] = str(model.silentMode)
        self._store["user-settings"]["show-beta-courses"] = str(model.showBetaCourses)

        with open(self._store_path, "w", encoding="utf-8") as cfg_file:
            self._store.write(cfg_file)

    def _createGroups(self):
        if "general" not in self._store:
            self._store["general"] = {}

        if "user-settings" not in self._store:
            self._store["user-settings"] = {}

    def _createStoreFile(self, store_name: str) -> Path:
        if not self._store_directory_path.is_dir():
            self._store_directory_path.mkdir()

        store_path = Path(self._store_directory_path, f"{store_name}.cfg")

        if not store_path.is_file():
            with open(store_path, "w", encoding="utf-8"):
                pass

        return store_path
