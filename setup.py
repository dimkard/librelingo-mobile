# SPDX-FileCopyrightText: 2023 Dimitris Kardarakos <dimkard@posteo.net>
# SPDX-License-Identifier: CC0-1.0

import subprocess
from typing import List, Tuple
from pathlib import Path
from setuptools import setup


def mo_data_files() -> list:
    """Compile po files, return a list of tuples that should be added to
    data_files"""

    po_dir_path = Path("glossaico/po")
    po_files = [x for x in po_dir_path.iterdir() if (x.is_file() and x.suffix == ".po")]

    mo_data_files_list: List[Tuple] = []
    translations_dir = "glossaico/mo"

    for po_file_path in po_files:
        locale_code = po_file_path.stem
        target_build_dir = Path(f"{translations_dir}/{locale_code}/LC_MESSAGES")

        if not target_build_dir.is_dir():
            target_build_dir.mkdir(parents=True)

        subprocess.run(
            f"pybabel compile -i {po_file_path} -o {target_build_dir}/glossaico.mo",
            check=True,
            shell=True,
        )

        mo_data_files_list.append(
            (
                f"share/locale/{locale_code}/LC_MESSAGES/",
                [f"{target_build_dir}/glossaico.mo"],
            ),
        )

    return mo_data_files_list


setup(
    data_files=[
        ("share/applications", ["org.codeberg.dimkard.glossaico.desktop"]),
        ("share/icons/hicolor/scalable/apps", ["org.codeberg.dimkard.glossaico.svg"]),
        ("share/metainfo", ["org.codeberg.dimkard.glossaico.metainfo.xml"]),
    ]
    + mo_data_files()
)
