# SPDX-FileCopyrightText: 2021 Dimitris Kardarakos <dimkard@posteo.net>
# SPDX-License-Identifier: GPL-3.0-or-later

from PySide2.QtCore import Signal, Property, QAbstractListModel, QModelIndex
from PySide2.QtCore import Qt


class QModulesModel(QAbstractListModel):
    """Class that provides a model of the modules of a course"""

    TitleRole = Qt.UserRole + 1
    SkillsRole = Qt.UserRole + 2

    def __init__(self, parent=None):
        super().__init__(parent)
        self._course_modules = []

    def roleNames(self):
        """Returns the model's role names"""
        return {
            QModulesModel.TitleRole: b"title",
            QModulesModel.SkillsRole: b"skills",
        }

    def rowCount(self, parent=QModelIndex()):
        """Returns the number of rows under the given parent"""
        if parent.isValid():
            return 0

        return len(self._course_modules)

    def data(self, index, role):
        """Returns the data stored under the given role for the item referred to
        by the index
        """
        if not index.isValid():
            return None

        row = self._course_modules[index.row()]
        result = "Invalid Role"

        if role == Qt.DisplayRole:
            result = row.title
        elif role == QModulesModel.TitleRole:
            result = row.title
        elif role == QModulesModel.SkillsRole:
            result = row.skills

        return result

    def readCourseModules(self) -> list:
        """Returns the list that represents the modules of a course"""
        return self._course_modules

    def setCourseModules(self, val: list):
        """Sets the list that represents the modules of a course"""
        self._course_modules = val
        # pylint: disable=no-member
        self.courseModulesChanged.emit()

    @Signal
    def courseModulesChanged(self):
        """Signal that is emitted when the course modules list property
        is set or changed
        """

    courseModules = Property(
        list, readCourseModules, setCourseModules, notify=courseModulesChanged
    )
