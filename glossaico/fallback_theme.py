# SPDX-FileCopyrightText: 2023 Dimitris Kardarakos <dimkard@posteo.net>
# SPDX-License-Identifier: GPL-3.0-or-later

import os
import logging


class FallbackTheme:
    """The fallback theme to be used"""

    def __init__(self, theme_dirs):
        self._theme_dirs = theme_dirs

    @property
    def name(self) -> str:
        """Returns the name fallback theme to be used.
        If breeze is installed, returns breeze. Otherwise,
        it returns glossaico_breeze"""

        fb_theme = "breeze" if self._themeExists("breeze") else "glossaico_breeze"
        logging.info("Fallback theme %s", fb_theme)

        return fb_theme

    def _themeExists(self, name: str):
        for p in self._theme_dirs:
            if os.path.isdir(p) and self.__class__.themeInDir(name, p):
                return True

        return False

    @staticmethod
    def themeInDir(theme_name: str, dir_name: str):
        """Checks if theme is found in the directory given"""
        for d in os.listdir(dir_name):
            if (d == theme_name) and os.path.isfile(f"{dir_name}/{d}/index.theme"):
                return True
        return False
