/*
 * SPDX-FileCopyrightText: 2021 Dimitris Kardarakos <dimkard@posteo.net>
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

import QtQuick 2.12
import QtQuick.Controls 2.12 as Controls
import QtQuick.Layouts 1.12
import QtMultimedia 5.12
import org.kde.kirigami 2.12 as Kirigami

Kirigami.Page {
    id: root

    property int challengesAnswered: 0

    title: QGlTr._("End of practice session")

    signal sessionEnd()

    Audio {
        id: audio

        source: QDirSettings.soundDir + "/fanfare.mp3"
        autoLoad: false
    }

    ColumnLayout {
        anchors {
            left: parent.left
            right: parent.right
            top: parent.top
        }

        spacing: Kirigami.Units.largeSpacing

        RowLayout {
            spacing: Kirigami.Units.largeSpacing

            Image {
                source: QDirSettings.iconDir + "/mascot-jetpack-noshadow"

                Layout.maximumHeight: Kirigami.Units.iconSizes.enormous
                Layout.preferredWidth: height
            }

            Kirigami.Heading {
                id: loaderHeader

                text: root.challengesAnswered > 0 ? QGlTr._("The practice session has been completed. You have practiced %1 challenges!").arg(root.challengesAnswered) : QGlTr._("The practice session has been canceled")
                wrapMode: Text.WordWrap
                level: 1

                Layout.fillWidth: true

            }

            Layout.fillWidth: true
        }
    }

    Component.onCompleted: {
        if(!QConfiguration.silentMode && (root.challengesAnswered > 0)) {
            audio.play()
        }
    }

    footer: MessageFooter {

        rightActions: [
            Kirigami.Action {
                text: QGlTr._("Continue")

                onTriggered: root.sessionEnd()
            }
        ]
    }
}
