/*
 * SPDX-FileCopyrightText: 2021 Dimitris Kardarakos <dimkard@posteo.net>
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

import QtQuick 2.12
import QtQuick.Controls 2.12 as Controls
import QtQuick.Layouts 1.12
import QtMultimedia 5.12
import org.kde.kirigami 2.12 as Kirigami
import org.codeberg.dimkard.glossaico 1.0 as Glossaico

AbstractChallengePage {
    id: root

    property string courseName

    challengeItem: columnLayout
    title: QGlTr._("Type what you hear")

    ColumnLayout {
        id: columnLayout

        width: parent.width
        spacing: Kirigami.Units.largeSpacing

        Kirigami.Heading {
            text: QGlTr._("Type what you hear")
            wrapMode: Text.WordWrap
            bottomPadding: Kirigami.Units.largeSpacing * 2
            leftPadding: Kirigami.Units.largeSpacing

            Layout.fillWidth: true
        }

        RowLayout {

            Audio {
                id: playAudio

                source: QDirSettings.voiceDir + "/" + root.challengeData.audio + ".mp3"
                autoLoad: false
            }

            Controls.Button {
                icon.name: "media-playback-start"
                onClicked: {
                    playAudio.play()
                    if(!Kirigami.Settings.isMobile) {
                        answerField.forceActiveFocus(Qt.ActiveWindowFocusReason)
                    }
                }
            }

            Controls.TextField {
                id: answerField

                focus: true
                Layout.fillWidth: true

                onTextEdited: root.answerGiven = { "answer" : text }
            }
            Layout.fillWidth: true
        }

        Flow {
            spacing: Kirigami.Units.largeSpacing

            Repeater {
                model: root.specialCharacters

                delegate: Controls.Button {
                    text: modelData

                    onClicked: {
                        answerField.insert(answerField.cursorPosition, modelData)
                        answerField.forceActiveFocus(Qt.MouseFocusReason)
                    }
                }
            }

            Layout.fillWidth: true
        }
    }

    Component.onCompleted: playAudio.play()
}
