/*
 * SPDX-FileCopyrightText: 2023 Dimitris Kardarakos <dimkard@posteo.net>
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

import QtQuick 2.7
import QtQuick.Controls 2.0 as Controls
import QtQuick.Layouts 1.0
import org.kde.kirigami 2.19 as Kirigami


Controls.ToolBar {
    id: root

    property string text
    property list<QtObject> leftActions
    property list<QtObject> rightActions
    property color backgroundColor: Kirigami.Theme.backgroundColor
    property color borderColor: Kirigami.Theme.disabledTextColor

    padding: Kirigami.Units.largeSpacing

    background: Rectangle {
        id: bg

        color: root.backgroundColor
        border.width: 0
        opacity: 0.8
    }

    RowLayout {
        id: contentLayout

        anchors.left: parent.left
        anchors.right: parent.right

        Controls.Label {
            id: message

            width: parent.width
            height: parent.height
            leftPadding: Kirigami.Units.smallSpacing
            rightPadding: Kirigami.Units.smallSpacing

            color: Kirigami.Theme.textColor
            wrapMode: Text.WordWrap
            elide: Text.ElideRight
            text: root.text
            verticalAlignment: Text.AlignVCenter
        }

        FooterActions {
            layoutAlignment: Qt.AlignLeft
            actions: root.leftActions
        }

        FooterActions {
            layoutAlignment: Qt.AlignRight
            actions: root.rightActions
        }
    }
}

