# SPDX-FileCopyrightText: 2022 Dimitris Kardarakos <dimkard@posteo.net>
# SPDX-License-Identifier: GPL-3.0-or-later

import pytest
from glossaico.q_courses_model import QCoursesModel


@pytest.fixture(scope="function", name="test_model")
def fixture_test_model(courses_json_path):
    q_courses_model = QCoursesModel(courses_json_path)

    return q_courses_model


def test_rowCount_inp_valid_model_load_beta_courses_out_returns_correct_amt(
    test_model,
):
    test_model.loadData(True)
    assert test_model.rowCount() == 2


def test_rowCount_inp_valid_model_load_stable_courses_out_returns_correct_amt(
    test_model,
):
    test_model.loadData(False)
    assert test_model.rowCount() == 1


def test_name_role_inp_valid_model_stable_only_out_correct_value(test_model):
    test_model.loadData(False)
    value = test_model.data(test_model.index(0, 0), QCoursesModel.NameRole)
    assert value == "spanish-from-english"


def test_target_role_inp_valid_model_beta_inlcuded_out_correct_value(test_model):
    test_model.loadData(True)
    value = test_model.data(test_model.index(1, 0), QCoursesModel.TargetRole)
    assert value == "French"


def test_source_role_inp_valid_model_stable_only_out_correct_value(test_model):
    test_model.loadData(False)
    value = test_model.data(test_model.index(0, 0), QCoursesModel.SourceRole)
    assert value == "English"


def test_description_role_inp_valid_model_stable_only_out_correct_value(test_model):
    test_model.loadData(False)
    value = test_model.data(test_model.index(0, 0), QCoursesModel.DescriptionRole)
    assert value == "Spanish for English speakers"


def test_deployed_role_inp_valid_model_stable_only_out_correct_value(test_model):
    test_model.loadData(False)
    value = test_model.data(test_model.index(0, 0), QCoursesModel.DeployedRole)
    assert value


def test_inProduction_role_inp_valid_model_stable_only_out_correct_value(test_model):
    test_model.loadData(False)
    value = test_model.data(test_model.index(0, 0), QCoursesModel.InProductionRole)
    assert value
