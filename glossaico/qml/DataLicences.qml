/*
 * SPDX-FileCopyrightText: 2023 Dimitris Kardarakos <dimkard@posteo.net>
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

import QtQuick 2.12
import QtQuick.Controls 2.12 as Controls
import QtQuick.Layouts 1.12
import org.kde.kirigami 2.12 as Kirigami

Kirigami.OverlaySheet {
    id: root

    property var licensesModel: QAppInfo.aboutData["dataLicenses"]

    title: QGlTr._("Licences")

    Controls.SwipeView {
        id: swipeView

        height: applicationWindow().height * 0.7
        Layout.preferredWidth: applicationWindow().width * 0.7

        currentIndex: tabBar.currentIndex
        clip: true

        Repeater {
            model: root.licensesModel

            Item {
                Controls.Label {
                    width: parent.width
                    wrapMode: Text.WordWrap
                    text: modelData.text
                }
            }
        }
    }

    footer: Controls.TabBar {
        id: tabBar

        position: Controls.TabBar.Footer
        currentIndex: swipeView.currentIndex

        Repeater {
            model: root.licensesModel

            Controls.TabButton {
                text: modelData.name
            }
        }
    }
}
