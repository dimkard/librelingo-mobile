# SPDX-FileCopyrightText: 2022 Dimitris Kardarakos <dimkard@posteo.net>
# SPDX-License-Identifier: GPL-3.0-or-later

import pytest
from sqlalchemy.orm import clear_mappers
from glossaico.practice_session_alchemy_store import PracticeSessionAlchemyStore
from glossaico.practice_session_alchemy_store_keeper import (
    PracticeSessionAlchemyStoreKeeper,
)
from glossaico.alchemy_database import AlchemyDatabase


@pytest.fixture(scope="module", name="db_engine")
def fixture_db_engine():
    db_loader = AlchemyDatabase("sqlite+pysqlite:///:memory:")
    clear_mappers()
    engine = db_loader.load()

    return engine


def test_context_manager_inp_use_as_context_manager_out_no_error(db_engine):
    keeper = PracticeSessionAlchemyStoreKeeper(db_engine)

    with keeper:
        print("In context manager")

    assert keeper


def test_store_inp_valid_store_keeper_out_valid_store(db_engine):
    keeper = PracticeSessionAlchemyStoreKeeper(db_engine)

    with keeper:
        assert isinstance(keeper.store(), PracticeSessionAlchemyStore)
