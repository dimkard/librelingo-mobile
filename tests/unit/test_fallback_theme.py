# SPDX-FileCopyrightText: 2022 Dimitris Kardarakos <dimkard@posteo.net>
# SPDX-License-Identifier: GPL-3.0-or-later

from pathlib import Path
import pytest
from glossaico.fallback_theme import FallbackTheme


@pytest.fixture(scope="function", name="theme_paths")
def fixture_theme_paths(tmp_path):
    """Returns the directories where paths should be found"""

    icon_theme_dir_1 = f"{tmp_path}/share/icons"
    icon_theme_dir_2 = f"{tmp_path}/local/share/icons"

    Path(icon_theme_dir_1).mkdir(parents=True)
    Path(icon_theme_dir_2).mkdir(parents=True)

    return [icon_theme_dir_1, icon_theme_dir_2, ":/icons"]


def test_name_inp_valid_breeze_exists_returns_breeze(theme_paths):
    breeze_dir = f"{theme_paths[0]}/breeze"
    Path(breeze_dir).mkdir(parents=True)
    with open(f"{breeze_dir}/index.theme", "w", encoding="utf-8") as f:
        f.write("")

    fallback_theme = FallbackTheme(theme_paths)
    assert fallback_theme.name == "breeze"


def test_name_inp_invalid_breeze_exists_returns_glossaico_breeze(theme_paths):
    theme_dir = f"{theme_paths[0]}/breeze"
    Path(theme_dir).mkdir()
    fallback_theme = FallbackTheme(theme_paths)
    assert fallback_theme.name == "glossaico_breeze"


def test_fallbackTheme_inp_no_breeze_exists_returns_glossaico_breeze(theme_paths):
    fallback_theme = FallbackTheme(theme_paths)
    assert fallback_theme.name == "glossaico_breeze"
