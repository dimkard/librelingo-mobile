/*
 * SPDX-FileCopyrightText: 2021 Dimitris Kardarakos <dimkard@posteo.net>
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

import QtQuick 2.12
import QtQuick.Controls 2.12 as Controls
import QtQuick.Layouts 1.12
import org.kde.kirigami 2.12 as Kirigami
import org.codeberg.dimkard.glossaico 1.0 as Glossaico

QtObject {
    id: root

    property var challengeData
    property var specialCharacters: []
    property string targetLanguage
    property string courseName

    property var cards: Component {
        id: cardsChallengePage

        CardsChallengePage {
            challengeData: root.challengeData
        }
    }

    property var options: Component {
        id: optionsChallengePage

        OptionsChallengePage {
            challengeData: root.challengeData
        }
    }

    property var shortInput: Component {
        id: shortInputChallengePage

        ShortInputChallengePage {
            challengeData: root.challengeData
            specialCharacters: root.specialCharacters
            targetLanguage: root.targetLanguage
        }
    }

    property var listeningExercise: Component {
        id: listeningChallengePage

        ListeningChallengePage {
            challengeData: root.challengeData
            specialCharacters: root.specialCharacters
        }
    }

    property var chips: Component {
        id: chipsChallengePage

        ChipsChallengePage {
            challengeData: root.challengeData
        }
    }
}
